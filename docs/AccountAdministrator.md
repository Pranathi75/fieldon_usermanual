﻿
# Account Administrator
<!-- {docsify-ignore} -->
Account Administrator is created and mapped to specific Account by Super Administrator, the activities which are created in a specific account are seen by the respective Account Administrator and Super Administrator.

<p align="center"> <img width="1000" height="500" src="media/AAA.jpg"> </p>

<div align="center"> 

**`Fig` Account administrator activities**

</div>

<p>Enter valid Account admin username and password and click on Sign in.</p>

Login as [Account Administrator ](https://demo.fieldon.com/#/login)

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa1.jpg"></p>

<div align="center">

**`Fig.1` Account Administrator login page**

</div>

- After login we can see the Account Admin page where we will have the 

 - <code>Administrators</code>	
 - <code>Users</code>
 - <code>Categories</code>	
 - <code>Devicemanagement</code>
 - <code>Templates</code>
 - <code>Forms</code>
 - <code>Projects</code>
 - <code>Tasks</code>
 - <code>Workflowmanagement</code>
 - <code>Downloads</code>
 - <code>Settings</code>

## Administrators

1. On click of Administrators tab we can see the list of Administrators available.
2. We can see all types of Administrators.

 i.	Account Administrator

 ii. Group Administrator

 iii. Moderators

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa2.jpg"></p>

<div align="center">

**`Fig.2` Administrators page**

</div>

3. We can filter the Administrator by using filter in the right-hand top. By default, it shows all administrators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa3.jpg"></p>

<div align="center">

**`Fig.3` Filter Administrators**

</div>

4. Select Moderator, shows all available moderators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa4.jpg"></p>

<div align="center">

**`Fig.4` Filter moderators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa5.jpg"></p>

<div align="center">

</div>

5. Select Group Administrators, shows all available group administrators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa6.jpg"></p>

<div align="center">

**`Fig.5` Filter Group administrators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa7.jpg"></p>

<div align="center">

</div>

6.  Search administrators in the search bar which is on top right-hand, shows the available administrators.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa8.jpg"></p>

<div align="center">

**`Fig.6` Search administrators**

</div>

### Administrator Create:
1.	Account Admin can create the Administrator by clicking on the create button in ellipse on the right-hand top.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa9.jpg"></p>

<div align="center">

</div>

2.	On click of create button a form will open with fields first name, last name, email, phone, role, select avatar. Fill the details and click on create.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa10.jpg"></p>

<div align="center">

**`Fig.7` Administrator creation form**

</div>

3.	We have Group Administrator and Moderators.
4.	Based on the check box selection Administrator will be created.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa11.jpg"></p>

<div align="center">

**`Fig.8` Group administrator creation**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa12.jpg"></p>

<div align="center">

</div>

5.	If role is not selected, then Moderator will be created.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa13.jpg"></p>

<div align="center">

**`Fig.9` Moderator creation**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa14.jpg"></p>

<div align="center">

</div>

<code>Note:</code>
No duplicate Emails allowed for same type of Administrator.

### Administrator Edit/Update:
1.	To edit Administrator, select the Administrator and hover on the card we can see Edit button.
2.	Select the Edit button, form will be opened with existing details, update the details, and click on Update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa15.jpg"></p>

<div align="center">

**`Fig.10` Edit administrators**

</div>

3.	After successful update, we can see message in the top right corner.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa16.jpg"></p>

<div align="center">

**`Fig.11` Edit form of administrators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa17.jpg"></p>

<div align="center">

</div>

### Administrator Delete:
1.	To delete Administrator, select the Administrator and click on delete button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa18.jpg"></p>

<div align="center">

</div>

2.	Once click on Delete button, application shows the conformation pop up, then select the yes option.
3.	After click on Yes button, application shows the successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa19.jpg"></p>

<div align="center">

**`Fig.12` Delete administrators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa20.jpg"></p>

<div align="center">

</div>

<code>Note:</code>
Can delete Administrators, only if it is not associated to accounts

### Administrator Password Reset:
1.	Super Admin can reset the password of Administrator to default password.
2.	Select the Administrator and click on password reset button.
3.	Default password will be mm@1234.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa21.jpg"></p>

<div align="center">

**`Fig.13` Reset password-administrators**

</div>

4.	Once click on Reset button, application shows the conformation pop up, then select the yes option.
5.	After click on Yes button, application shows the successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa22.jpg"></p>

<div align="center">

**`Fig.14` Reset password-administrators**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa23.jpg"></p>

<div align="center">

</div>

## Users
1.	On click of Users tab we can see the list of Users available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa24.jpg"></p>

<div align="center">

**`Fig.15` Users list**

</div>

2.	Admin can create the users individually or can import users from excel sheet by clicking Import users option
3.	For creating the user, click on actions ellipse.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa25.jpg"></p>

<div align="center">

</div>

### Individual User Creation
- Click on Create User option.
- Fill the required details and click on Create button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa26.jpg"></p>

<div align="center">

**`Fig.16` Create Users**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa27.jpg"></p>

<div align="center">

**`Fig.16` User creation form**

</div>

- Once click on Create button, application shows the successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa28.jpg"></p>

<div align="center">

</div>

<code>Note:</code> 
 Duplicate emails are not allowed.

### User Edit/Update:
Admin can edit/ update the user profiles.
  1.To edit the user profile, first select the user profile and click on Edit button.
  2.  Make the changes and click on Update button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa29.jpg"></p>

<div align="center">

**`Fig.18` Edit users**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa30.jpg"></p>


<div align="center">

**`Fig.19` Edit users form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa31.jpg"></p>

<div align="center">

</div>

### User Delete:
1.	To delete User, select the User and click on delete button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa32.jpg"></p>

<div align="center">

**`Fig.20` Delete user**

</div>

2.	Once click on Delete button, application shows the conformation pop up, then select the yes option.
3.	After click on Yes button, application shows the successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa33.jpg"></p>

<div align="center">

**`Fig.21` Delete users**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa34.jpg"></p>

<div align="center">

</div>

<code>Note:</code>
 Can delete users, only if user is not assigned to tasks.

### User Password Reset:
1.	Account Administrator can reset the password of User to default password.
2.	Select the User and click on password reset button.
3.	Default password will be mm@1234.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa35.jpg"></p>

<div align="center">

**`Fig.22` Reset password**

</div>

4.	Once click on Reset button, application shows the conformation pop up, then select the yes option.
5.	After click on Yes button, application shows the successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa36.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa37.jpg"></p>

<div align="center">

</div>

### Import Users:
Import Users function defines, admin can create bulk user profiles within a single moment by uploading the excel sheet. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa38.jpg"></p>

<div align="center">

**`Fig.23` Import Users**

</div>

1.	To import Users from excel sheet, first download User creation template from Import Users page and fill the user details (valid email ids). 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa39.jpg"></p>

<div align="center">

**`Fig.24` Import Users**

</div>

2.	Save the file with extension .xlsx (Only XLSX files are supported)

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa40.jpg"></p>

<div align="center">

</div>

3.	Select the excel in choose file and click on show table to check the details and edit the details.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa41.jpg"></p>

<div align="center">

**`Fig.25` Import Users**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa42.jpg"></p>

<div align="center">

</div>

4.	We can edit and delete the data by selecting Edit and Delete buttons.
5.	Click on the Upload button to upload the users.
6.	After successful upload we get a successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa43.jpg"></p>

<div align="center">

</div>

## Categories
1.	When clicked on Categories tab, we can see the list of Categories available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa44.jpg"></p>

<div align="center">

**`Fig.26` List of categories**

</div>

2.	Search categories, shows available categories with searched name on search bar and click on enter.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa45.jpg"></p>

<div align="center">

**`Fig.27` Search categories**

</div>

### Category Creation
1.	 On the right-hand top corner, we can see ellipse. 
2.	On Click of create button in ellipse we can create new Category.
3.	A form will be opened with name and description.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa46.jpg"></p>

<div align="center">

**`Fig.28` Create category**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa47.jpg"></p>

<div align="center">

**`Fig.29` Create category**

</div>

4.	Fill the details and click the create button.
5.	After successful creation we can see a toast message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa48.jpg"></p>

<div align="center">

</div>

<code>Note:</code>
Duplicate names not allowed.

### Category Edit/Update
1.	To edit Category hover on the category, we can see the buttons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa49.jpg"></p>

<div align="center">

**`Fig.30` Edit categories**

</div>

2.	Click on Edit button, view the existing data, and make the changes In description and update.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa50.jpg"></p>

<div align="center">

**`Fig.31` Edit category**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa51.jpg"></p>

<div align="center">

</div>

### Category Delete
1.	To delete Category hover on the category, we can see the buttons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa52.jpg"></p>

<div align="center">

**`Fig.32` Delete category**

</div>

2.	Once click on Delete button, application shows the conformation pop up, then select the yes option.
3.	After click on Yes button, application shows the successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa53.jpg"></p>

<div align="center">

**`Fig.33` Delete category**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa54.jpg"></p>

<div align="center">

</div>

<code>Note:</code>
 Can delete category, only if it is not assigned to forms/tasks.

## Device management
1.	When clicked on device management tab, we can see device details and activity. By default, device details page opens. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa55.jpg"></p>

<div align="center">

**`Fig.34` Device details in device management page**

</div> 

2. Account admin can search devices with model, username

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa56.jpg"></p>

<div align="center">

**`Fig.35` Search device details**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa57.jpg"></p>

<div align="center">

</div> 

3. Account admin can filter the device details by status. Click on filter icon and select the required status (All/ approved/pending/suspended/unauthorized/rejected) shows device details of selected status. By default, all device details are shown.
 - Any user trying to login form any device for the first time, needs his device to be approved by the account administrator.
 - When the request for a new device comes, its status is Pending. Account administrator needs to check for the credibility and take necessary action.
 - We get the list of Pending devices from the device icon in the header.
 - Account administrator can Approve/Suspend/Revoke/Unauthorized a particular device request based on the credibility.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa58.jpg"></p>

<div align="center">

**`Fig.36` Filter device details**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa59.jpg"></p>

<div align="center">

**`Fig.37` Filtered approved devices**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa60.jpg"></p>

<div align="center">

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa61.jpg"></p>

<div align="center">

**`Fig.38` Filtered pending device**

</div> 

4. Click on refresh icon to refresh the device details.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa62.jpg"></p>

<div align="center">

**`Fig.39` Refresh device details**

</div> 

5.Click on Activity, activity page opens. Select From date and To date and click on filter icon, shows the device activity details which are in between the selected dates.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa63.jpg"></p>

<div align="center">

**`Fig.40` Activity page in device management**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa64.jpg"></p>

<div align="center">

**`Fig.41` Filter Activity between From date and To date**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa65.jpg"></p>

<div align="center">

**`Fig.42` View Activity**

</div> 

## Templates
1.	When clicked on Templates tab, we can see the list of Templates available.
2.	Account admin can search templates, shows the available templates with searched name.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa66.jpg"></p>

<div align="center">

**`Fig.43` Search Template**

</div> 

3.	Filter templates, click on filter icon, select the type (All/public/private) shows the templates based on the selection, by default shows all templates.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa67.jpg"></p>

<div align="center">

**`Fig.44` Filter Template**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa68.jpg"></p>

<div align="center">

**`Fig.45` Filter Templates by public type**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa69.jpg"></p>

<div align="center">

</div> 

4.	Group by category, click on group by category icon, shows categories and templates under each category.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa70.jpg"></p>

<div align="center">

**`Fig.46` Group by category of Templates**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa71.jpg"></p>

<div align="center">

**`Fig.47` View Templates under each category**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa72.jpg"></p>

<div align="center">

</div> 

### Preview Template
5.	Click on preview icon, opens preview template page

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa73.jpg"></p>

<div align="center">

**`Fig.48` Preview of template**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa74.jpg"></p>

<div align="center">

**`Fig.49` Preview of template**

</div> 

### Import Template as Form
6.	 To import a template as form, go to the template and hover on the template we can see Import button. 
7.	Click on it to import the Template.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa75.jpg"></p>

<div align="center">

**`Fig.50` Click on import**

</div> 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa76.jpg"></p>

<div align="center">

**`Fig.51` Import template as form**

</div> 

8.	We can see the existing data of the template where we need to change name, select form type (public/private), When we select form type as private, we need to select users),select the category in that Account and click on Go button. Shows imported successfully message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa77.jpg"></p>

<div align="center">

</div> 

9.	Template will be imported into Forms tabs. Shows forms tabs after imported successfully.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa78.jpg"></p>

<div align="center">

</div> 

## Forms

1.	When clicked on Forms tab, we can see the list of Forms available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa79.jpg"></p>

<div align="center">

**`Fig.52` List of form**

</div> 

2.	Search forms, shows the forms with the searched input if any. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa80.jpg"></p>

<div align="center">

**`Fig.53` Search forms**

</div> 

3.	Filter forms, click on filter icon, select type All/public/private, shows forms based on the selection.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa81.jpg"></p>

<div align="center">

**`Fig.54` Filter forms**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa82.jpg"></p>

<div align="center">

**`Fig.55` Private forms**

</div>

4.	GroupBy category, click on group by category shows categories and forms under each category.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa83.jpg"></p>

<div align="center">

**`Fig.56` Group by category of forms**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa84.jpg"></p>

<div align="center">

**`Fig.57` Group by category of forms**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa85.jpg"></p>

<div align="center">

**`Fig.58` Shows forms under a category**

</div>

5.	Account Administrator can create Forms. 

### Form Creation
1.	 On the right-hand top corner, we can see ellipse. 
2.	Click on ellipse, click on create 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa86.jpg"></p>

<div align="center">

**`Fig.59` Create forms**

</div>

3.	Create form will be opened with name, category, Form Type, Description and Design from Excel

 a.	<code>Private:</code> If private selected, then the form will be visible to only selected Users

 b.	<code>Public:</code> If public selected, then the form will be visible to all Users of that Account.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa87.jpg"></p>

<div align="center">

**`Fig.60` Create form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa88.jpg"></p>

<div align="center">

**`Fig.61` Create form**

</div>

4.	After clicking of ‘Go’ button. It will open ‘Form Builder’ where we can select the widgets and create Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa89.jpg"></p>

<div align="center">

**`Fig.62` Form widgets**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa90.jpg"></p>

<div align="center">

**`Fig.63` Media widgets and Addon widgets**

</div>

5.	We need to drag and drop the widgets to the palette to create Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa91.jpg"></p>

<div align="center">

</div>

6.	After drag and drop of widget, we need to fill the properties of the widget and need to save the properties.

7.	After saving of the widget we need to save the Form by clicking the save button in the top right corner.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa92.jpg"></p>

<div align="center">

</div>

8.	After clicking of the 'save' button we will get a popup with a drop down where we have to select the Display fields and save the Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa93.jpg"></p>

<div align="center">

**`Fig.64` Create form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa94.jpg"></p>

<div align="center">

</div>

<code>Note:</code>
Duplicate names not allowed.

### Form creation from excel
1.	We can create form widgets from excel by uploading excel in Form creation page.
2.	Download ‘creation Template’ from the creation page 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa95.jpg"></p>

<div align="center">

**`Fig.65` Form creation using excel**

</div>

3.	After filling the Excel, we need to upload the excel by clicking Choose File button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa96.jpg"></p>

<div align="center">

</div>

4.	After uploading click on 'Go' button, we can see the Form Builder with widgets.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa97.jpg"></p>

<div align="center">

</div>

5.	We can update the details and save the form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa98.jpg"></p>

<div align="center">

</div>

### Form Preview
1.	To Preview the Form, go to the form and hover on the form, we can see preview button,
Click on preview button a popup will be open with all the widgets in the form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa99.jpg"></p>

<div align="center">

**`Fig.66` Form preview**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa100.jpg"></p>

<div align="center">

**`Fig.67` Form preview**

</div>

### Form Edit/Update
1.	 To edit a Form hover on the Form, we can see Edit button.
2.	On Click of edit button on the form we can see the existing details.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa101.jpg"></p>

<div align="center">

**`Fig.68` Edit form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa102.jpg"></p>

<div align="center">

**`Fig.69` Edit form**

</div>

3.	After clicking of 'Go' button. It will open Form Builder with the existing widgets and can add new widgets to the Form.
4.	We need to drag and drop the widgets to the palate to add new widgets to Form.
5.	After drag and drop of widget, we need to fill the properties of the widget and need to save the properties.
6.	After saving of the widget we need to save the Form by clicking the 'save' button in the top right corner.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa103.jpg"></p>

<div align="center">

</div>

7.	After clicking of the save button we will get a popup with a drop down where we have to select the Display fields and click on update will update the Form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa104.jpg"></p>

<div align="center">

**`Fig.70` Form Update**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa105.jpg"></p>

<div align="center">

</div>

###  Form Derived Conditions
1.	To add derived conditions to a form, after clicking on edit instead of Save click on  "Save" and "Continue".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa106.jpg"></p>

<div align="center">

</div>

2.	After clicking on, we can see a side bar opened with Save, Add and Back buttons.
3.	Once click on Add we can see the dropdown to select and set a condition.
4.	Click on Set to set the condition and Apply.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa107.jpg"></p>

<div align="center">

**`Fig.71` Derived conditions of form**

</div>

5.	After that we can see the conditions in a list where we can delete and save the complete list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa108.jpg"></p>

<div align="center">

</div>

6.	Click on Save to save the form.

### Form Export to Excel
1.	To export form to excel hover on the form, click on export to excel icon, downloads the form in excel.
2.	We can Export the Form to Excel to get all the widgets of the form available in the form in excel.
3.	We can use fill this excel and upload in Work Assignments to user.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa109.jpg"></p>

<div align="center">

**`Fig.72` Export to excel-Form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa110.jpg"></p>

<div align="center">

**`Fig.73` Excel file of form**

</div>

### Submitted records of form
1.	Click on submitted records icon which in on hover on the form. Shows records within 24 hrs.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa111.jpg"></p>

<div align="center">

**`Fig.74` Submitted records of form**

</div>

2.	Click on "filter" icon, opens a filter records popup.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa112.jpg"></p>

<div align="center">

**`Fig.75` Filter records**

</div>

3.	Select from date, to date and select users, click on get data.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa113.jpg"></p>

<div align="center">

**`Fig.76` Filter submitted records of form**

</div>

4.	Shows submitted records table which are in between selected dates.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa114.jpg"></p>

<div align="center">

</div>

5.	Export to Excel, select records and click on export to excel, opens columns selections page in the right side. Select the columns and click on ✅. Shows confirmation message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa115.jpg"></p>

<div align="center">

**`Fig.77` Export to excel-Submitted records of form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa116.jpg"></p>

<div align="center">

**`Fig.78` Columns selection page**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa117.jpg"></p>

<div align="center">

</div>

6.	Export to mail, select records and click on "export to mail button", opens a popup with name email attachment with fields To, CC, select pdf or excel and click on send. Shows confirmation message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa118.jpg"></p>

<div align="center">

**`Fig.79` Export to Mail**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa119.jpg"></p>

<div align="center">

**`Fig.80` Email attachment form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa120.jpg"></p>

<div align="center">

</div>

7.	Export to pdf, select records and click on export to pdf button, opens columns selection page with list of columns. Select columns and click on✅. Shows confirmation message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa121.jpg"></p>

<div align="center">

**`Fig.81` Export to excel-Submitted records form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa122.jpg"></p>

<div align="center">

</div>

8.	Inserted locations, select record and click on inserted location button, map opens and shows the inserted location of the selected record.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa123.jpg"></p>

<div align="center">

**`Fig.82` Inserted locations-Submitted records form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa124.jpg"></p>

<div align="center">

</div>

- Click on disable map view icon, to disable the map.

9.	Add columns, click on add columns buttons, open columns selection page on the right side with list of columns. Select required columns and click on ✅. Selected columns are shown in the records table. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa125.jpg"></p>

<div align="center">

**`Fig.83` Add columns-Submitted records form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa126.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa127.jpg"></p>

<div align="center">

**`Fig.84` Add columns**

</div>

<code>Note:</code>
 - Can export 1000 records to Excel at a time, for more than 1000 records shows alert message.
 - Can export 10 records to mail/pdf at a time, for more than 10 records shows alert message.
yy
### Share Link - form
1.	To share a form link, we need to select the Form and hover on the form, we can see the ‘Share Link’ button. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa128.jpg"></p>

<div align="center">

**`Fig.85` Share link of form**

</div>

2.	After clicking share link button, we can see a popup with validity, we need to select the expiry date and click on Generate button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa129.jpg"></p>

<div align="center">

</div>

3.	After clicking of generate we can see the Generated ‘Link’ and expiry date. we can copy the link by clicking on copy button

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa130.jpg"></p>

<div align="center">

**`Fig.86` Copy link of form**

</div>

4.	Paste the copied link in the new tab, opens the form

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa131.jpg"></p>

<div align="center">

</div>

### Info - Form
1.	To see information of a Form hover on the Form, we can see info button. Click on the info button.
2.	Opens a popup with information of the form in a table.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa132.jpg"></p>

<div align="center">

**`Fig.87` Info-form**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa133.jpg"></p>

<div align="center">

**`Fig.88` Info-form**

</div>

### Form Version History
1.	We can view list of changes done on the form after creation as Version History.
2.	To view the version history of a Form, hover on the form and click Version History button

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa134.jpg"></p>

<div align="center">

**`Fig.89` Version history-form**

</div>

3.	After clicking on that we can view the list of versions of that form.
4.	Click on the ‘Info’ icon to view the changes done on that form in that version.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa135.jpg"></p>

<div align="center">

</div>

5.	A popup will open with Added, Updated and Deleted categories.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa136.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa137.jpg"></p>

<div align="center">

**`Fig.89` Version history details**

</div>

### Business rules

1. On Clicking on Forms on the left-hand side menu bar, we can see list of forms available.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa307.jpg"></p>

<div align="center">

**`Fig.90` List of forms**

</div>

2. To business rules, select the form and hover on it we can see business rules option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa308.jpg"></p>

<div align="center">

**`Fig.91`Set rules forms**

</div>

3. Select the business rules option, red flag rules will be opened with already created rule if any or will show No rules defined message and Add new rule option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa309.jpg"></p>

<div align="center">

**`Fig.92` Add new rule**

</div>

4.Clik on Add new rule option, blank row will be added with 'Filed', 'Rule' and dropdowns, 'Value' field and save rule, delete rule option in Action.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa310.jpg"></p>

<div align="center">

**`Fig.93`Add new rule**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa311.jpg"></p>

<div align="center">

**`Fig.94` Add new rule**

</div>

#### Set rule for Text Box

1 On selection Field of type Text Box from field dropdown, in rule dropdown Equalto,Notequal to, Black options will be visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa312.jpg"></p>

<div align="center">

**`Fig.95` Set rules for Text box field**

</div>

2 On selection rule as Equaltp/NotEqual to, Super admin will be able to enter text in valid field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa313.jpg"></p>

<div align="center">

**`Fig.96` Set rules for text box field**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa314.jpg"></p>

<div align="center">

**`Fig.97` Set rules for Text box field**

</div>

3. On selection Rule as Blank, Vlaue field will be disabled

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa315.jpg"</p>

<div align="center">

**`Fig.98` Set rules for Text box field**

</div>

4. After choosing field, Rule and Value click on save rule option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa316.jpg"></p>

<div align="center">

**`Fig.99` Set rules for Text box field**

</div>

5. Application shows toast message as "Rule saved successfully".

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa317.jpg"></p>

<div align="center">

**`Fig.100` Saved rules for Text box field**

</div>

6. In redflag rules page, Rule will be shown in the list.

#### Set rule for Text Area field

1.	On Selecting Field of type Text Area from Field dropdown, in Rule Dropdown Equal to, Not Equal to, Blank options will be visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa318.jpg"></p>

<div align="center">

**`Fig.101` Set rule for Text area field**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa319.jpg"></p>

<div align="center">

**`Fig.102`   Set rule for Text area field**                         

</div>

2.	On selecting Rule as Equal to/ Not Equal to, super admin will be able to enter text in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa320.jpg"></p>

<div align="center">

**`Fig.103`  Save rule for area field**

</div>

3.	On selecting Rule as Blank, value field will be disabled.
4.	After choosing field, rule and value click on save rule option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa321.jpg"></p>

<div align="center">

**`Fig.104` Save rule for area field**

</div>

5.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa322.jpg"></p>

<div align="center">

**`Fig.105` Saved rule for area field**

</div>

6.In red flag rules page, Rule will be shown in the list.

#### Set rule for Number field

1.	On Selecting Field of type Number from Field dropdown, in Rule dropdown less than, greater than, Equal to, Blank options will be visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa323.jpg"></p>

<div align="center">

**`Fig.106` Set rule for Number field**

</div>

2.	On selecting Rule as Greater than/less than/ Equal to, super admin will be able to enter text in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa324.jpg"></p>

<div align="center">

**`Fig.107` Set rule for Number field**

</div>

3.	On selecting Rule as Blank, value field will be disabled.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa325.jpg"></p>

<div align="center">

**`Fig.108` Save rule for number field**

</div>

4.	After choosing field, rule and value click on save rule option.
5.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa326.jpg"></p>

<div align="center">

**`Fig.109` Saved rule for number field**

</div>

6.	In red flag rules page, Rule will be shown in the list.

#### Set rule for Dropdown field

1.	On Selecting Field of type Dropdown from Field dropdown, in Rule dropdown contains, not contains, Blank options will be visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa327.jpg"></p>

<div align="center">

**`Fig.110` Set rule for Dropdown field**

</div>

2.	On selecting Rule as contains/not contains, super admin will be able to view and select options of the selected dropdown in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa328.jpg"></p>

<div align="center">

**`Fig.111` Set rule for Dropdown field**

</div>

3.	On selecting Rule as Blank, value field will be disabled.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa329.jpg"></p>

<div align="center">

**`Fig.112` Save rule for Dropdown field**

</div>

4.	After choosing field, rule and value click on save rule option.
5.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa330.jpg"></p>

<div align="center">

**`Fig.113` Saved rule for Dropdown field**

</div>

6.	In red flag rules page, Rule will be shown in the list.

#### Set rule for Radio field

1.	On Selecting Field of type Radio from Field dropdown, in Rule dropdown contains, not contains, Blank options will be visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa331.jpg"></p>

<div align="center">

**`Fig.114` Set rule for Radio field**

</div>

2.	On selecting Rule as contains/not contains, super admin will be able to view and select options of the selected radio in value field.
3.	On selecting Rule as Blank, value field will be disabled.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa332.jpg"></p>
<div align="center">

**`Fig.115` Set rule for Radio field**

</div>

4.	After choosing field, rule and value click on save rule option.
5.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa333.jpg"></p>

<div align="center">

**`Fig.116` Saved rule for Radio field**

</div>

6.	In red flag rules page, Rule will be shown in the list.

#### Set rule for Check box field

1.	On Selecting Field of type checkbox from Field dropdown, in Rule dropdown contains, Not contains, Blank options will be visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa334.jpg"></p>

<div align="center">

**`Fig.117` Set rule for Check box field**

</div>

2.	On selecting Rule as contains/not contains, super admin will be able to view and select options of the selected radio in value field.
3.	On selecting Rule as Blank, value field will be disabled. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa335.jpg"></p>

<div align="center">

**`Fig.118` Set rule for Check box field**

</div>

4.	After choosing field, rule and value click on save rule option. 
5.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa336.jpg"></p>

<div align="center">

**`Fig.119` Saved rule for check box field**

</div>

6.	In red flag rules page, Rule will be shown in the list.

#### Set rule for Calendar field

1.	On Selecting Field of type calendar from Field dropdown, in Rule dropdown greater than, less than options will be visible.
2.	On selecting Rule as greater than/less than super admin will be able to view and select date in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa337.jpg"></p>

<div align="center">

**`Fig.120` Set rule for calendar field**

</div>

3.	After choosing field, rule and value click on save rule option. 
4.	Application shows toast message as “Rule saved successfully”.
5.	In red flag rules page, Rule will be shown in the list.

#### Set rule for Calculator field

1.	On Selecting Field of type checkbox from Field dropdown, in Rule dropdown Greater than, less than, equal to, Blank options will be visible.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa338.jpg"></p>

<div align="center">

**`Fig.121` Set rule for calculator field**

</div>

2.	On selecting Rule as greater than/less than/equal to, super admin will be able to view and enter the number in value field.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa339.jpg"></p>

<div align="center">

**`Fig.122` Save rule for calculator field**

</div>

3.	On selecting Rule as Blank, value field will be disabled. After choosing field, rule and value click on save rule option. 
4.	Application shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa340.jpg"></p>

<div align="center">

**`Fig.123` Saved rule for calculator field**

</div>

5.	In red flag rules page, Rule will be shown in the list.

#### Edit rule

1.	All the rules will be in editable mode, make changes by selecting the field, rule and value and click on save rule icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa341.jpg"></p>

<div align="center">

**`Fig.124` Edit rule**

</div>

2.	New changes will be saved successfully and shows toast message as “Rule saved successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa342.jpg"</p>

<div align="center">

**`Fig.125` Edit rule**

</div>

#### Delete rule

1.	Click on delete icon, application will show confirmation action message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa343.jpg"></p>

<div align="center">

**`Fig.126` Delete rule**

</div>

2.	On clicking on yes, rule will be deleted successfully.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa344.jpg"></p>

<div align="center">

**`Fig.127` Delete rule**

</div>

3.	Shows toast message as “Rule deleted successfully "and rule will be removed from the list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa345.jpg"></p>

<div align="center">

**`Fig.127` rule deleted**

</div>

### Form Delete
1.	To delete ‘Form’ hover on the form, we can see the buttons.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa138.jpg"></p>

<div align="center">

**`Fig.128`  Delete form**

</div>

2.	Once click on ‘Delete’ button, application shows the confirmation pop up, then select the ‘yes’ option.
3.	After click on ‘Yes’ button, application shows the successful message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa139.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa140.jpg"></p>

<div align="center">

</div>

## Projects
1.	Click on projects icon, opens projects page showing list of projects.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa141.jpg"></p>

<div align="center">

**`Fig.129`  List of projects**

</div>

2.	Search projects, Shows projects available with the searched input.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa142.jpg"></p>

<div align="center">

**`Fig.130`  Search projects**

</div>

3.	Grid view, to view projects as grids click on the icon in the right corner.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa143.jpg"></p>

<div align="center">

**`Fig.131` Grid view of projects**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa144.jpg"></p>

<div align="center">

</div>

### Project Creation
Projects are defined as the admin can be able to create Projects and assign to Group admin, where Group admin creates certain project tasks and assign work assignment to group of users along with some predefined data.
1.	For creating Projects first go to “Projects” tab and click on ellipse, click on ‘Create’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa145.jpg"></p>

<div align="center">

**`Fig.132` Create projects**

</div>

2.	After clicking on ‘Create’ button, now enter the project details, like ‘Name, Description, Category, Group Admin, start date & End date’ and then click on ‘Create’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa146.jpg"></p>

<div align="center">

</div>

3.	The application will display a message saying, ‘Project created successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa147.jpg"></p>

<div align="center">

</div>

### Edit Project
Admin can be able to edit/update the created Project (if required) by clicking on ‘Edit’ option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa148.jpg"></p>

<div align="center">

**`Fig.133`  Edit project**

</div>

- The details like ‘Description, End date’ will be available for updating.
- After updating the details click on ‘Update button’ to save the changes.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa149.jpg"></p>

<div align="center">

**`Fig.134` Create projects**

</div>

- Once after clicking on the update button, the application will display a message saying, ‘Project updated successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa150.jpg"></p>

<div align="center">

</div>

### Delete Project
To delete the Project, select the Project and click on ‘Delete’ button. Admin can delete the project only if no project task is assigned.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa151.jpg"></p>

<div align="center">

**`Fig.135`  Delete projects**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa152.jpg"></p>

<div align="center">

</div>

- If any task is associated with the Project, then admin cannot delete.
- Successful pop-up will be shown when Project is deleted.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa153.jpg"></p>

<div align="center">

</div>

### Project Task
 - The project task within the project will be created by Group administrator assigned to the project. Workflow of this functionality will be same as Task.
 - Click on project tasks, shows list of projects tasks under the project which are created by assigned Group administrator.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa154.jpg"></p>

<div align="center">

**`Fig.136`  Project tasks **

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa155.jpg"></p>

<div align="center">

**`Fig.137` List of project tasks **

</div>

- Search project tasks, shows projects tasks with searched input.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa156.jpg"></p>

<div align="center">

**`Fig.138` Search project tasks **

</div>

- Preview of project tasks, shows existing details of project tasks as read only fields.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa157.jpg"></p>

<div align="center">

**`Fig.139` Preview of project tasks **

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa158.jpg"></p>

<div align="center">

</div>

- Work assignments, click on work assignments icon in project task page, shows list of work assignments under the project task.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa159.jpg"></p>

<div align="center">

**`Fig.140` Work assignment of project tasks **

</div>

- Submitted records, click on submitted records icon shows records of work assignment.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa160.jpg"></p>

<div align="center">

**`Fig.141`  Submitted records of work assignment **

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa161.jpg"></p>

<div align="center">

**`Fig.142`  Submitted records of work assignment **

</div>

### Submitted Records

- Once all the records are displayed, Admin can be able to export the data to Excel, mail, PDF, inserted location and add columns.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa162.jpg"></p>

<div align="center">

</div>

### Export to Excel
- To download the data to Excel format, select the records individually or bulk and click on the ‘Export Excel’ button from tabular view.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa163.jpg"></p>

<div align="center">

**`Fig.143` Export to excel-Submitted records**

</div>

- Once after clicking on excel option, now all the list of fields will be displayed select the green color option. The application will display a message saying, ‘Please check the downloaded records in downloads tab’

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa164.jpg"></p>

<div align="center">

**`Fig.144`  Column selections page to export records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa165.jpg"></p>

<div align="center">

</div>

- The downloaded Excel appears in this format.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa166.jpg"></p>

<div align="center">

**`Fig.145` Downloaded excel doc of submitted records**

</div>

### Export to Email
- Select records and click on Export to Email option. Opens email attachment popup.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa167.jpg"></p>

<div align="center">

**`Fig.146` Export to email-Submitted records**

</div>

- Administrator can email the records to the Valid Email & can also send the same to another email by adding in the Email Id in CC which is optional. Select Excel/pdf and click on send

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa168.jpg"></p>

<div align="center">

**`Fig.147`  Email attachment form**

</div>

- Opens columns selection page with list of all columns, selects the columns and click on ✅. Successful pop-up will be shown.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa169.jpg"></p>

<div align="center">

**`Fig.148`  Columns selection page-Submitted records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa170.jpg"></p>

<div align="center">

</div>

### Export to PDF
- Administrator will be able to select single or multiple records and then click on Pdf option to generate the submitted data to PDF format.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa171.jpg"></p>

<div align="center">

**`Fig.149`  Export to pdf-Submitted records**

</div>

- Once after clicking on pdf option, columns selection page opens with the list of all columns, select the columns and click on ✅. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa172.jpg"></p>

<div align="center">

**`Fig.150` Column selection page-Submitted records**

</div>

- The application will display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa173.jpg"></p>

<div align="center">

</div>

- Downloaded pdf appears in the format shown below.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa174.jpg"></p>

<div align="center">

</div>

### Inserted locations
- Select the records and then click on ‘Inserted Locations’. Locations from where the records were submitted will be displayed as a marker on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa175.jpg"></p>

<div align="center">

**`Fig.151` Inserted locations-Submitted records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa176.jpg"></p>

<div align="center">

</div>

### Add columns
Click on add columns, open columns selection page with list of all columns. Select the columns and click on ✅

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa177.jpg"></p>

<div align="center">

**`Fig.152`  Add columns-Submitted records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa178.jpg"></p>

<div align="center">

**`Fig.153` Columns selection page**

</div>

The selected columns information will be updated in the table of Submitted records.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa179.jpg"></p>

<div align="center">

</div>

### Preview of submitted records
Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa180.jpg"></p>

<div align="center">

**`Fig.154` Preview of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa181.jpg"></p>

<div align="center">

**`Fig.155` Preview of submitted record **

</div>

Click on actions, shows actions like sketching, geo tagged images.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa182.jpg"></p>

<div align="center">

**`Fig.156` Preview of submitted record **

</div>

Click on sketching, to see the sketching done by user in map

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa183.jpg"></p>

<div align="center">

**`Fig.157` Sketching**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa184.jpg"></p>

<div align="center">

**`Fig.158`  Sketching**

</div>

Click on geo tagged images, to see the images that are geo tagged by user on map.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa185.jpg"></p>

<div align="center">

**`Fig.159`  Geo tagged images of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa186.jpg"></p>

<div align="center">

**`Fig.160` Geo tagged images**

</div>

Click on attachments to see the attached files. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa187.jpg"></p>

<div align="center">

**`Fig.161`  Attachments of submitted record**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa188.jpg"></p>

<div align="center">

</div>

### Work flow history
For the project tasks that are associated with workflow, there are two types of work flow levels: 1. Task level
  1. Task level
  2. Record level 

<code>1.Task level:</code>

To view Work flow history for project tasks that are associated with work flow in task level, click on the work flow history icon on the project task hover.

Opens work flow history page with work flow details, status, comments. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa189.jpg"></p>

<div align="center">

**`Fig.162` Workflow history**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa190.jpg"></p>

<div align="center">

**`Fig.163` Workflow history**

</div>

To view work flow history of project tasks that are assigned to record level work flow, in submitted records preview, click on actions and click on work flow history icon. 

Opens work flow history page with work flow details, status, comments.

<code>2. Record level:</code>

- In record level each record will have work flow.
- Click on work assignments of project task with record level work flow.
- Click on submitted records icon, shows submitted records of work assignments.
- Click on record status of a record, shows preview of the submitted record.
- Click on actions icon.
- On clicking on actions icon, shows options work flow history, sketching, geo tagged images.
- Click on work flow history icon.
- Shows work flow history page showing work flow details, status and comments.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa191.jpg"></p>

<div align="center">

**`Fig.164` Workflow history**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa192.jpg"></p>

<div align="center">

**`Fig.165` Workflow history**

</div>

### Re-assign the data
Re-assign function defines that, if any record contains invalid or incomplete data then admin will be able to re-assign the data to any user (if required).

To re-assign the record, first select the required record, and click on ‘Re-assign’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa193.jpg"></p>

<div align="center">

**`Fig.166` Reassign records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa194.jpg"></p>

<div align="center">

**`Fig.167`  Reassign records**

</div>

Once the record is re-assigned, then the record status will be changed to Reassigned record.

## Tasks
Tasks are defined as the admin can be able to assign the work to group of users along with some predefined data (if required).
 - For assigning the task, first go to ‘Tasks tab’ and click on ellipse, click on create

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa195.jpg"></p>

<div align="center">

**`Fig.168` Create task**

</div>

- After clicking on ‘Create’ button, now enter the task details, like ‘Name, Description, Category, Group administrator, Workflow, Workflow Level, Start date, End date’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa196.jpg"></p>

<div align="center">

**`Fig.169` Create task form**

</div>

- Once the details are entered now click on ‘Create’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa197.jpg"></p>

<div align="center">

**`Fig.170` Create tasks**

</div>

- Once the task is created application shows a pop-up message saying, “Task created successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa198.jpg"></p>

<div align="center">

</div>

### Work assignment creation
- After creating the task click on ‘Work assignment option’ to assign the users.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa199.jpg"></p>

<div align="center">

**`Fig.171` Work assignment creation**

</div>

- Now click on ‘+ button’ which is to create a new work assignment.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa200.jpg"></p>

<div align="center">

**`Fig.172` Work assignment creation**

</div>

- Once after clicking on new work assignment creation, the application will ask whether to upload pre-populated data ‘Yes or No’.

### Work assignment creation- without pre-populated data

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa201.jpg"></p>

<div align="center">

</div>

- Now click on No and continue.
- Opens work assignment creation form, now enter the details like ‘Assignment name, Form, Frequency, Users, Start date, End date’.
- Once after entering all the details now click on create button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa202.jpg"></p>

<div align="center">

**`Fig.173` Work assignment without pre-papulated data**

</div>

- Once after clicking on create button the application will display a message saying, Work assignment created successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa203.jpg"></p>

<div align="center">

</div>

### Work assignment creation – with pre-populated data

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa204.jpg"></p>

<div align="center">

**`Fig.174` Work assignment creation with pre-papulated data**

</div>

- Click on yes, and continue.
- Opens work assignment creation form, now enter the details like ‘Assignment name, Form, Frequency, Users, Start date, End date’.
- Select the form and upload the selected form Excel file with pre-populated data.
- Once after entering all the details now click on next button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa205.jpg"></p>

<div align="center">

**`Fig.175` Work assignment creation with pre-papulated data**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa206.jpg"></p>

<div align="center">

**`Fig.176` Assign pre-papulated data to users**

</div>

- Once after clicking on create button the application will display a message saying, Work assignment created successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa207.jpg"></p>

<div align="center">

</div>

### Edit Task
- Admin can edit/ update the created task (if required) by clicking on ‘Edit’ option. Admin can update few details like “Description, Category, Group Admin, End date”. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa208.jpg"></p>

<div align="center">

**`Fig.177` Edit task**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa209.jpg"></p>

<div align="center">

</div>

<code>Note:</code>Admin cannot update the assigned Users, assigned Forms, assigned data.

- After editing the details for task click on ‘Update button’ to save the changes. The application will display a message saying, ‘Task updated successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa210.jpg"></p>

<div align="center">

</div>

### Task Deletion

- Administrator will be able to delete the task only if the start date of the task is not current date, or if no users are assigned to the Task.

- If the task is in-progress, then administrator will not be able to delete the task & application will display a message saying, ‘Task is in progress cannot be deleted’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa211.jpg"></p>

<div align="center">

</div>

### Submitted Records
1.	Submitted records define to the admin, that all the user submitted records will be displayed. Admin can export the data to PDF, Excel.
2.	To get the submitted data click on the required task and then ‘Work assignment’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa212.jpg"></p>

<div align="center">

**`Fig.178` List of tasks**

</div>

3.	Now within the work assignment click on ‘Submitted records.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa213.jpg"></p>

<div align="center">

**`Fig.179`  List of work assignments**

</div>

4.	All the submitted data by the users for the work assignment will be displayed as a list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa214.jpg"></p>

<div align="center">

**`Fig.180` Submitted records of work assignments**

</div>

5.	Once all the records are displayed, Admin can be able to export the records to excel, PDF, Attach to Email, Re-assign data, Inserted locations, add columns.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa215.jpg"></p>

<div align="center">

</div>

### Export to PDF
1.	Administrator will be able to select single or multiple records and then click on export to Pdf option to generate the submitted data to PDF format.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa216.jpg"></p>

<div align="center">

**`Fig.181` Export to pdf-Submitted records**

</div>

2.	Once after clicking on pdf option, now all the list of fields will be displayed select the green color option. 
3.	The application will display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa217.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa218.jpg"></p>

<div align="center">

</div>

### Export to Excel
To download the data to Excel format, select the records individually or bulk and click on the ‘Export to Excel’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa219.jpg"></p>

<div align="center">

**`Fig.182` Export to excel-Submitted records**

</div>

Once after clicking on excel option, columns selection page with list of all columns. Select the columns and click on ✅. 

The application will display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa220.jpg"></p>

<div align="center">

**`Fig.183` Columns selection page**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa221.jpg"></p>

<div align="center">

</div>

### Export to Email
Administrator can email the records to the Valid Email & can also send the same to another email by adding in Cc which is optional. Select the records and click on export to email

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa222.jpg"></p>

<div align="center">

**`Fig.184`  Export to email-Submitted records**

</div>

1.	After clicking on export to Email option, opens email attachment popup. Enter valid Email, select Excel/pdf and click on send.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa223.jpg"></p>

<div align="center">

</div>

2.	Opens columns selection page with list of columns, select columns and click on ✅.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa224.jpg"></p>

<div align="center">

**`Fig.185`  Columns selection page**

</div>

3.	Successful pop-up will be shown on performing email records as Mail sent successfully.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa225.jpg"></p>

<div align="center">

</div>

### Re-assign the records
- Re-assign function defines that, if any record contains invalid or incomplete data then admin will be able to re-assign the data to any user (if required).
- To re-assign the record, first select the required record, and click on ‘Re-assign’ button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa226.jpg"></p>

<div align="center">

**`Fig.186` Reassign records**

</div>

- Now enter comments and select users and click on submit button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa227.jpg"></p>

<div align="center">

**`Fig.187` Reassign records**

</div>

- Application shows message as “Records reassigned successfully”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa228.jpg"></p>

<div align="center">

</div>

<code>Note:</code>Once the record is re-assigned, then the record status will be changed to Reassigned record.

### Inserted locations
First, select the records individually or multiple records and then click on ‘Inserted Locations’. Shows the locations from where the selected records are submitted.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa229.jpg"></p>

<div align="center">

**`Fig.188` Inserted location-Submitted records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa230.jpg"></p>

<div align="center">

**`Fig.189`  Inserted location-Submitted records**

</div>

### Add columns
- Click on add columns icon in submitted records page of work assignments, opens columns selection page with list of all columns.
- Select the columns and click on ✅.
- The selected columns and information of the columns are updated to the table of submitted records.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa231.jpg"></p>

<div align="center">

**`Fig.190` Add columns-Submitted records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa232.jpg"></p>

<div align="center">

**`Fig.191`  Columns selection page**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa233.jpg"></p>

<div align="center">

</div>

### Pending Records
- Click on pending records button which is on the work assignments hover.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa234.jpg"></p>

<div align="center">

**`Fig.192` Pending records**

</div>

- After clicking on pending records, shows pending records with the pre-populated data. 

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa235.jpg"></p>

<div align="center">

</div>

### Edit assigned records
- Admin can add other user to the pending records by selecting the records and select users in the dropdown and click on add user icon. Click on save records. Shows updated successfully message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa236.jpg"></p>

<div align="center">

**`Fig.193` Assign Pending records to user**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa237.jpg"></p>

<div align="center">

</div>

### Delete assigned records
- By selecting records and clicking on delete icon, assigned records can be deleted by admin.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa238.jpg"></p>

<div align="center">

**`Fig.194` Delete records**

</div>

- Open popup to confirm, click on yes.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa239.jpg"></p>

<div align="center">

</div>

- After clicking on yes, the record deleted successfully. Shows Updated successfully message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa240.jpg"></p>

<div align="center">

</div>

### Add records
- Admin can add records, to add click on +icon, shows choose file option, upload file with pre-populated data of the same form. 
- Select users, click on add user icon. 
- Shows updated successfully message.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa241.jpg"></p>

<div align="center">

**`Fig.195` Add records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa242.jpg"></p>

<div align="center">

**`Fig.196` Add records**

</div>

### Assign records

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa243.jpg"></p>

<div align="center">

**`Fig.197` Assign added records to user**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa244.jpg"></p>

<div align="center">

**`Fig.198` Save records to assign**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa245.jpg"></p>

<div align="center">

</div>

The assigned records for a task will be able to Export to excel, pdf.

### Export to excel
- Administrator will be able to export the pending records to excel.
-  First, select the records and then click on Excel option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa246.jpg"></p>

<div align="center">

**`Fig.199` Export to excel-Pending records**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa247.jpg"></p>

<div align="center">

</div>

- Once after clicking on export to excel option, columns selection page open showing list of all columns. Select the columns and click on✅. 
- Shows message to check the Excel file in downloads tab.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa248.jpg"></p>

<div align="center">

</div>

- The exported records in excel will be downloaded in downloads tab.

### Export to PDF
- Administrator will be able to export the pending records to PDF.
- First, select the records and then click on export to PDF option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa249.jpg"></p>

<div align="center">

**`Fig.200` Export to pdf**

</div>

- Once after clicking on Export to PDF option, columns selection page open showing list of all columns. Select the columns and click on✅. 
- Shows message to check the pdf in downloads tab.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa250.jpg"></p>

<div align="center">

</div>

- The exported data in PDF will be downloaded in downloads tab.

### Export to Email
- Administrator will be able to export the pending records to Email option.
- First, select the records and then click on Export to Email option.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa251.jpg"></p>

<div align="center">

**`Fig.201` Export to email**

</div>

- Once after clicking on Email option, email attachment popup opens, Enter the to & CC valid email Id’s. Now select the required format ‘Pdf, Excel’ and then click on submit button.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa252.jpg"></p>

<div align="center">

**`Fig.202` Email attachment form**

</div>

- Columns selection page open showing list of all columns. Select the columns and click on✅.
- Shows message Email sent successfully.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa253.jpg"></p>

<div align="center">

</div>

## Workflow Management
Workflow Management is the feature provided by FieldOn which we can coordinate and sequence the tasks.

### Workflow creation
- On clicking the “Workflow Management” tab, we can see the list of workflows created previously.
- Click on the “Create” button which would open a page to enter details for workflow creation.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa254.jpg"></p>

<div align="center">

**`Fig.203` Create workflow**

</div>

- Fill the required details, you can add the levels, and then create the workflow.

- After successful creation you can see the created workflow in the list.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa255.jpg"></p>

<div align="center">

**`Fig.204` Create workflow**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa256.jpg"></p>

<div align="center">

**`Fig.205`  Workflow creation**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa257.jpg"></p>

<div align="center">

</div>

### Workflow Edit and level deletion
- You can edit a workflow if the workflow is not yet assigned to any task or form.

- Click on “Edit” button and you can change the “From” and “To” values and click on save. Updates the workflow.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa258.jpg"></p>

<div align="center">

**`Fig.206` Edit workflow**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa259.jpg"></p>

<div align="center">

</div>

- If the workflow is not yet assigned to any task or form, the level of the workflow can also be deleted.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa260.jpg"></p>

<div align="center">

</div>

### Workflow deletion
- If the workflow is not assigned to any task or form, it can be deleted.
- Click on the “Delete” button and, on confirmation message click “Yes”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa261.jpg"></p>

<div align="center">

**`Fig.207` Delete workflow**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa262.jpg"></p>

<div align="center">

**`Fig.208` Delete workflow**

</div>

- The workflow will be deleted. The application shows message as “workflow deleted successfully”.

### Workflow Approval Cycles
 We have two types of approval cycles for workflow in FieldOn.
 - Task level workflow
 - Record level workflow

<code>Task level Workflow:</code>

The “Task Level Workflow” is the one assigned on the “Tasks”, the administrators/moderators in the workflow are responsible to approve the whole task by checking the submissions on it.
- While creating a task, we assign workflow to it as follows,

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa263.jpg"></p>

<div align="center">

</div>

- Select the type of workflow.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa264.jpg"></p>

<div align="center">

**`Fig.209` Workflow selection**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa265.jpg"></p>

<div align="center">

**`Fig.210`  Workflow level selection**

</div>

- After you have created the task, and assigned the workflow to it, we can now create assignments for this task.
- For creation of assignment please check the topic “Work Assignments”.
- The data is submitted on these assignments by the field user.
- After the data is submitted, the Account Administrator, needs to change the status of the task, to “Workflow Cycle Started”, then the workflow would be triggered and the task would be pending for approval of the first person in the workflow.
- Example: The following is the workflow used for depicting the task level workflow.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa266.jpg"></p>

<div align="center">

**`Fig.211  Edit status**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa267.jpg"></p>

<div align="center">

**`Fig.212`  No.of days waiting for approval**

</div>

<code>Workflow Cycle Started:</code>
- As in the above workflow, “Chanda GA” is the first person, he will find this task in the tasks pending for his approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa268.jpg"></p>

<div align="center">

**`Fig.213`  Workflow approval**

</div>

- On clicking on the above highlighted icon, you can find the recent task “Ballarpur Area” pending for his approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa269.jpg"></p>

<div align="center">

**`Fig.214`  Approval process**

</div>

- Now if the “Chanda GA” rejects the task, all the work assignments under it and corresponding records in it are reassigned to the users.
- When users, submit all the reassigned records for all work assignment under the particular task, the workflow starts again, and the task is again pending for approval of “Chanda GA”
- Now the “Chanda GA” approves and the task goes to next level in the workflow and as you can see in the below screenshot, on approval it would go to “Moderator Chandrapur” (who is a moderator).

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa270.jpg"></p>

<div align="center">

**`Fig.215` Approval process**

</div>

- Now the task comes to the next level, which is at “Moderator Chandrapur”.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa271.jpg"></p>

<div align="center">

**`Fig.216` Approval process**

</div>

- “Moderator Chandrapur” can accept or reject the task, if moderator accepts, it goes to the next level, else back to the earlier level.
- Now on accepting, it goes to “Chanda GA” (Note: Please see the from and to for all levels in the workflow)

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa272.jpg"></p>

<div align="center">

</div>

- Again, the “Chanda GA” will be responsible to approve or reject the task.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa273.jpg"></p>

<div align="center">

</div>

- On approving the workflow cycle completes.

<code>Record level Workflow:</code>

The “Record Level Workflow” is the one assigned on the “Tasks/Forms”, the administrators/moderators in the workflow are responsible to approve the record by checking the submissions on it.
Here we take an example of Record Level Workflow on a form. (Note: We have used same workflow as used in task level workflow)
- We assign workflow to a form by editing the form.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa274.jpg"></p>

<div align="center">

</div>

- Whenever any record is submitted for this form, the workflow gets triggered and the record goes to the person at the first level of the workflow. You can find the records submitted for forms by clicking on “Submitted Record” icon.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa275.jpg"></p>

<div align="center">

</div>

- Now login as “Chanda GA” and go to the forms records, you will find the record status in RED color. The RED color indicates that the record is pending for the logged in user’s approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa276.jpg"></p>

<div align="center">

</div>

- On clicking on the status, the record details open in the side panel. There on clicking on the ellipse, you will find accept or reject icons.

### Approval flow – record level

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa277.jpg"></p>

<div align="center">

**`Fig.217` Record level approval**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa278.jpg"></p>

<div align="center">

**`Fig.218`  Record level approval**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa279.jpg"></p>

<div align="center">

</div>

- On rejecting the record, it goes back to the field user for resubmission.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa280.jpg"></p>

<div align="center">

</div>

- When field user submits the data, again the record comes to the “Chanda GA” for approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa281.jpg"></p>

<div align="center">

</div>

- Now when “Moderator Chandrapur” logs in, he can find the record in RED color in forms records, waiting for his approval.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa282.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa283.jpg"></p>

<div align="center">

</div>

- The next level person, “Chanda GA”, now has to accept or reject the record.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa284.jpg"></p>

<div align="center">

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa285.jpg"></p>

<div align="center">

</div>

## Downloads
- The “Downloads” tab provides the feasibility to download the record reports, that is both excel and pdf reports.
- It has the other details like File name, generated on, size of file, downloaded from, type of record, from date, to date, task name, download option.
- The files under “Downloads” tab gets deleted automatically after 24 hours of download.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa286.jpg"></p>

<div align="center">

**`Fig.219` Downloads page**

</div>

## Settings
Administrator will be able to perform some of the actions from the system. The below is the list
- Profile
- Privileges
- Configurations
- Reference List
- Application Settings

### Profile
- Administrator will be able to change the user details like ‘First name, Last name, Email, Phone Number, Image’.
- First, click on the profile and then edit the required details and then click on save option. The changes will be updated with new ones.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa287.jpg"></p>

<div align="center">

**`Fig.220`  Profile settings**

</div>

- The application will display a message saying, ‘Profile Updated Successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa288.jpg"></p>

<div align="center">

</div>

### Privileges
The administrator will be able to update the privileges for ‘Group administrator & Moderators’.

### Group administrator Privileges
- First, select the Group administrator and then select the admin name all the list of functionalities for the group administrator will be displayed.
- For the functionalities which are in view state, administrator will be able to change the state to Edit. Whereas for the functionalities which are in Edit state, administrator will be able to change the state to View.
- Toggle On & Off to change the states.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa289.jpg"></p>

<div align="center">

**`Fig.221` Previllege settings**

</div>

### Moderator Privileges
- First, select the Moderator and then select the admin name all the list of functionalities for the Moderator will be displayed.
- For the functionalities which are in view state, administrator will be able to change the state to Edit. Whereas for the functionalities which are in Edit state, administrator will be able to change the state to View.
- Toggle On & Off to change the states.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa290.jpg"></p>

<div align="center">

**`Fig.222` Previllege settings**

</div>

### Configurations
- The administrator will be able to auto approve the devices at a single stretch by clicking on auto approve option.
- First, select the configurations and then click on auto approve devices. All the devices will be auto approved for the account.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa291.jpg"></p>

<div align="center">

**`Fig.223` Configuration-settings**

</div>

### Reference List
- First click on the reference list and then application will ask to Upload excel data.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa292.jpg"></p>

<div align="center">

**`Fig.224` Reference list creation**

</div>

- Select the required excel sheet having the drop-down fields and its values.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa293.jpg"></p>

<div align="center">

**`Fig.225` Reference list creation**

</div>

- Once after selecting the valid file now click on Submit option so that the uploaded sheet will be updated in the system. The application will display a message saying ‘Reference list created successfully’.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa294.jpg"></p>

<div align="center">

</div>

- Administrator will be able to add or Edit the uploaded values in the system. 

### Application Settings
- Administrator will be able to update the account lock or lock interval for the users who are accessing the application by entering the invalid details for few attempts.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa295.jpg"></p>

<div align="center">

**`Fig.226` Application Settings **

</div>

- First, select the account lock, lock interval, and then click on update button. For account block enter the value and for account interval enter the value and then save the values.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa296.jpg"></p>

<div align="center">

**`Fig.227` Application Settings **

</div>

## Dashboards
The Dashboards provide the statistical information present under the particular login. Following are the different roles under the application and their respective dashboards.
Throughout all the pages, you can find the dashboard icon present in the right middle of the screen edge.

### Account Administrator:
Under Account Administrator we have following entities:
- Account Specific
- Projects
- Tasks
- Users
- Device Management

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa297.jpg"></p>

<div align="center">

**`Fig.228`  Dashboards**

</div>

- Under the “Account Specific” entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa298.jpg"></p>

<div align="center">

**`Fig.229` Account specific-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa299.jpg"></p>

<div align="center">

**`Fig.230` Account specific-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa300.jpg"></p>

<div align="center">

</div>

- Under the “Projects” entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa301.jpg"></p>

<div align="center">

**`Fig.231`  Projects-Dashboards**

</div>

- Under the “Tasks” entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa302.jpg"></p>

<div align="center">

**`Fig.232` Tasks-Dashboards**

</div>

- Under the “Users” entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa303.jpg"></p>

<div align="center">

**`Fig.233` Users-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa304.jpg"></p>

<div align="center">

</div>

- Under the “Device Management” entity the following are the statistics captured.

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa305.jpg"></p>

<div align="center">

**`Fig.234` Devicemanagement-Dashboards**

</div>

<p align = "center"><img width = "10000 "height ="450" src = "media/AA/aa306.jpg"></p>

<div align="center">

</div>















