
# What is FieldOn?

> GO DIGITAL FOR DYNAMIC DATA COLLECTION For more info visit https://fieldon.com/

# Features

> Easily capture explicate image data with any smart device. Real-time accessing and storing of data is a cake walk with photo capture.

> Supervisors can control the field from multiple locations with user tracking and real time data . Scan QR and barcodes from your device into the forms.

> Expedite client approvals, sign contracts and agreements on time.Notify your team once work is completed and send work order copy on-the-fly.

> Use timestamp features to show ehen work was performed, how long it took and when it was completed. Get all the history you need to manage assets and report time and work accurately.

> Customise your forms to suit your business type using our administrative dashboards. Export, evaluate and review collected data and make timely, informed decisions.

> Enable your users to access data across departments with admins and sub-admins. Empower users to create and customise their forms and improve functionality with user-groups for different departments.