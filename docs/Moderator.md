# Moderator
<!-- {docsify-ignore} -->

Moderators are those who approves and rejects the task submitted by field users. 

<p align="center"> <img width="1000" height="500" src="media/MA.jpg"> </p>

<div align="center"> 

**`Fig` Moderator activities**

</div>

<p>Enter valid Moderator username and password and click on Sign in.</p>

Login as [Moderator ](https://demo.fieldon.com/#/login)

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod1.png"></p>

<div align="center">

**`Fig.1` Moderator login page**

</div>

<p>After login we can see the moderator page where we will have the.</p>

 - Forms
 - Approvals
 - Downloads

<hr>


## Forms

1. When clicked on Forms tab, we can see the list of Forms available.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod2.png"></p>

<div align="center">

**`Fig.2` Forms page**

</div>

2.Search forms, shows the forms with the searched input if any.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod3.png"></p>

<div align="center">

**`Fig.3` Search forms**

</div>

3.Filter forms, click on filter icon, select type All/public/private, shows forms based on the selection. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod4.png"></p>

<div align="center">

**`Fig.4` Filter forms**

</div>

4. GroupBy category, click on group by category shows categories and forms under each category. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod5.png"></p>

<div align="center">

**`Fig.5` Group By category-forms**
</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod6.png"></p>

<div align="center">

**`Fig.6` Group By categories-forms**
</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod7.png"></p>

<div align="center">

**`Fig.7` Forms under each category**

</div>

### Form preview

1.To Preview the Form, go to the form and hover on the form, we can see preview button, Click on preview button a popup will be open with all the widgets in the form. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod8.png"></p>

<div align="center">

**`Fig.8` Preview of form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod9.png"></p>

<div align="center">

**`Fig.9` Preview of form**

</div>

### Form Export to Excel

1. We can Export the Form to Excel to get all the widgets of the form available in the form of excel. 
2. We can use fill this excel and upload in Work Assignments to user.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod10.png"></p>

<div align="center">

**`Fig.10` Export to Excel-form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod11.png"></p>


### Submitted records- Form

1. Click on submitted records icon which in on hover on the form. Shows records within 24 hrs. 
2. Click on filter icon, opens a filter records popup. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod12.png"></p>

<div align="center">

**`Fig.11` Filter submitted records of forms**

</div>

3. Select from date, to date and select users, click on get data. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod13.png"></p>

<div align="center">

**`Fig.12` Filter records**

</div>

4.Shows submitted records table which are in between selected dates. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod14.png"></p>

<div align="center">

**`Fig.13` Filter records**

</div>

5.Export to Excel, select records and click on export to excel, opens columns selections page in the right side. Select the columns and click on ✅. Shows confirmation message. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod15.png"></p>

<div align="center">

**`Fig.14` Export to excel-Submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod16.png"></p>

<div align="center">

**`Fig.15` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod17.png"></p>

<div align="center">

</div>

6.Export to mail, select records and click on export to mail button, opens a popup with name email attachment with fields To, CC, select pdf or excel and click on send. Shows confirmation message. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod18.png"></p>

<div align="center">

**`Fig.16` Export to Email**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod19.png"></p>

<div align="center">

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod20.png"></p>

<div align="center">

**`Fig.17` Email attachment form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod21.png"></p>

<div align="center">

</div>

7.Export to pdf, select records and click on export to pdf button, opens columns selection page with list of columns. Select columns and click on ✅. Shows confirmation message. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod22.png"></p>

<div align="center">

**`Fig.18` Export to pdf**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod23.png"></p>

<div align="center">

**`Fig.19` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod24.png"></p>

<div align="center">

</div>

8.Inserted locations, select record and click on inserted location button, map opens and shows the inserted location of the selected record. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod25.png"></p>

<div align="center">

**`Fig.20` Inserted locations**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod26.png"></p>

<div align="center">

</div>

9.Add columns, click on add columns buttons, open columns selection page on the right side with list of columns. Select required columns and click on ✅. Selected columns are shown in the records table.  

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod27.png"></p>

<div align="center">

**`Fig.21` Add columns**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod28.png"></p>

<div align="center">

**`Fig.22` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod29.png"></p>

<div align="center">

**`Fig.23` Add columns**

</div>

**Note**

Can export 1000 records to Excel at a time, for more than 1000 records shows alert message. 

Can export 10 records to mail/pdf at a time, for more than 10 records shows alert message. 

### Info-Form

To see information of a Form hover on the Form, we can see info button. Click on the info button. 

1.Opens a popup with information of the form in a table. 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod30.png"></p>

<div align="center">

**`Fig.24` Info-form**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod31.png"></p>

<div align="center">

**`Fig.25` Info-form**

</div>

### Workflow Approval Cycles

We have two types of approval cycles for workflow in FieldOn.

 - Task level work flow,
 - Record level work flow


#### Task Level Workflow:

The “Task Level Workflow” is the one assigned to the “Tasks”, the administrators/moderators in the workflow are responsible to approve the whole task by checking the submissions on it.

Here we take an example of Task Level Workflow on a task. (Note: We have used same workflow as used in task level workflow). 

 - After user has submitted the assigned records, the workflow gets triggered and the task goes to the person at the first level of the workflow. You can find the records submitted for forms by clicking on “Submitted Record” icon.
 
 - Basically, work flow starts from group admin to moderator. After group admin has approved the task after checking the submitted records of that task’s work assignment, moderator will be able to approve or reject that task in approvals tab. 

 - Now when “Moderator” logs in, can find the task in approvals tab, showing as waiting for approval. 
 
 - After moderator approves the records of task’s work assignment, next approver in the work flow will get access to accept or reject records.

 - If the first person of the work flow rejects the task, it will be reassigned to user.

#### Record Level Workflow:

The “Record Level Workflow” is the one assigned on the “Tasks”, the administrators/moderators in the workflow are responsible to approve the record by checking the submissions on it.

.Here we take an example of Record Level Workflow on a task.

 - Whenever any record is submitted for this task’s work assignment, the workflow gets triggered and the record goes to the person at the first level of the workflow. Can find the records submitted for work assignment of the task by clicking on “Submitted Record” icon.

 - Basically, work flow starts from group admin to moderator. After group admin has approved the records of a task’s work assignment which is assigned to record level work flow, moderator will be able to approve or reject records of that task’s work assignment in approvals tab.

 - Now when “Moderator” logs in, can find the record in RED color in forms records, waiting for approval. 

 - After moderator approves the records of task’s work assignment, next approver in the work flow will get access to accept or reject records.

 - If the record is rejected by first approval in the work flow, it will be reassigned to user.

## Approvals

Click on approvals, opens approvals tab showing list of tasks (records level, task level) that are assigned to workflow with moderator waiting/ pending for the approval.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod32.png"></p>

<div align="center">

**`Fig.26` Approvals list**

</div>

### Approval flow – task level

- Moderator can accept task by clicking on accept task icon in approvals of the task, opens page with signature, comments fields.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod33.png"></p>

<div align="center">

**`Fig.27` Accept task**

</div>

- To accept task upload signature and comments and click on update.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod34.png"></p>

<div align="center">

**`Fig.28` Approve task**

</div>

## Rejection flow – task level

- Moderator can reject task, to reject task click on reject task icon opens a page with signature and comments.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod35.png"></p>

<div align="center">

**`Fig.29` Reject task**

</div>

- Upload signature, comments and click on update the task rejected successfully. When the task is rejected then task moves to previous approver.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod36.png"></p>

<div align="center">

**`Fig.30` Reject task**

</div>

- Click on workflow history icon to view work flow details, status, comments.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod37.png"></p>

<div align="center">

**`Fig.31` Workflow history of task with tasklevel workflow**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod38.png"></p>

<div align="center">

**`Fig.32` Workflow history**

</div>

- Click on work assignments to view work assignments of task/ project.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod39.png"></p>

<div align="center">

</div>

- Click on submitted records icon, to view submitted records of work assignments.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod40.png"></p>

<div align="center">

**`Fig.33` Submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod41.png"></p>

<div align="center">

</div>

### Export to Excel

- To download the data to Excel format, select the records individually or bulk and click on the ‘Export Excel’ button from tabular view.	 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod42.png"></p>

<div align="center">

**`Fig.34` Export to excel**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod43.png"></p>

<div align="center">

**`Fig.35` Columns selection page**

</div>


- Once after clicking on excel option, now all the list of fields will be displayed select the green color option. The application will display a message saying, ‘Please check the downloaded records in downloads tab’

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod44.png"></p>

<div align="center">

</div>

### Export to Email

- Select records and click on Export to Email option. Opens email attachment popup.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod45.png"></p>

<div align="center">

**`Fig.36` Export to email**

</div>

- Administrator can email the records to the Valid Email & can also send the same to another email by adding in the Email Id in CC which is optional. Select Excel/pdf and click on send.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod46.png"></p>

<div align="center">

</div>

- Opens columns selection page with list of all columns, selects the columns and click on ✅. Successful pop-up will be shown.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod47.png"></p>

<div align="center">

**`Fig.37` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod48.png"></p>

<div align="center">

</div>

### Export to PDF
- Administrator will be able to select single or multiple records and then click on Pdf option to generate the submitted data to PDF format.

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod49.png"></p>

<div align="center">

**`Fig.38` Export to pdf**

</div>

- Once after clicking on pdf option, columns selection page opens with the list of all columns, select the columns and click on ✅. The application will display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod50.png"></p>

<div align="center">

**`Fig.39` Columns selection page**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod51.png"></p>

<div align="center">

</div>

### Inserted locations

- Select the records and then click on ‘Inserted Locations’. Locations from where the records were submitted will be displayed as a marker on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod52.png"></p>

<div align="center">

**`Fig.40` Inserted locations**

</div>

### Add columns

- Click on add columns, open columns selection page with list of all columns. Select the columns and click on ✅ 

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod53.png"></p>

<div align="center">

**`Fig.41` Add columns**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod55.png"></p>

<div align="center">

**`Fig.42` Columns selection page**

</div>

- The selected columns information will be updated in the table of Submitted records.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod56.png"></p>

<div align="center">

</div>

### Preview of submitted records

- Click on the record, opens the page with details for preview and attachments of the record. In details for preview page shows the data submitted by user.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod57.png"></p>

<div align="center">

**`Fig.43` Submitted record of task with record level workflow**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod58.png"></p>

<div align="center">

**`Fig.44` Preview of record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod59.png"></p>

<div align="center">

</div>
Click on actions, shows actions like accept task, reject task, work flow history, sketching, geo tagged images for the task that is assigned with record level work flow.

Shows actions like sketching, geo tagged images also.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod60.png"></p>

<div align="center">

</div>

### Approval flow- record level
Click on accept task, opens page with to upload approver signature and comments. After uploading signature, comments click on update. After clicking on update, shows a message as task accepted successfully.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod61.png"></p>

<div align="center">

**`Fig.45` Accept record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod62.png"></p>

<div align="center">

**`Fig.46` Approved record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod63.png"></p>

<div align="center">

</div>

### Rejection flow- record level

Click on reject task, opens page with to upload approver signature and comments. After uploading signature, comments click on update. After clicking on update, shows a message as task rejected successfully.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod64.png"></p>

<div align="center">

**`Fig.47` Reject record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod65.png"></p>

<div align="center">

**`Fig.48` Reject record**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod66.png"></p>

<div align="center">

</div>

Click on work flow history, opens page click on view details to view the work flow details, comments, status.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod67.png"></p>

<div align="center">

**`Fig.49` Workflow history for record level**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod68.png"></p>

<div align="center">

</div>

Click on sketching, to see the sketching done by user in map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod69.png"></p>

<div align="center">

**`Fig.50` Sketching**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod70.png"></p>

<div align="center">

</div>

Click on geo tagged images, to see the images that are geo tagged by user on map.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod71.png"></p>

<div align="center">

**`Fig.51` Geo taggged images**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod72.png"></p>

<div align="center">

</div>

To edit the submitted records, enable the toggle to edit and make changes and click on update. Shows message updated successfully.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod73.png"></p>

<div align="center">

**`Fig.52` Edit submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod74.png"></p>

<div align="center">

**`Fig.53` Edit submitted records**

</div>

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod75.png"></p>

<div align="center">

</div>

## Downloads 
The “Downloads” tab provides the feasibility to download the record reports, that is both excel and pdf reports.

It has the details like file name, generated on, size of file, downloaded from, type of record, from date, to date, task name, download option.

The files under “Downloads” tab gets deleted automatically after 24 hours of download.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod76.png"></p>

<div align="center">

**`Fig.54` Downloads**

</div>

## Dashboards:

The Dashboards provide the statistical information present under the particular login. Following are the different roles under the application and their respective dashboards.

Throughout all the pages, you can find the dashboard icon present in the right middle of the screen edge.

### Moderator:

Under moderator type of entity:

- Approvals

Approvals page showing no of pending tasks with details end date, start date, task name.

<p align = "center"><img width = "1000 "height ="450" src = "media/moderator/mod77.png"></p>

<div align="center">

**`Fig.55` Dashboard-Approvals**

</div>






