![logo](media/FieldOnLogo.png ':size=200%')

# FieldOn User Manual<small>v.0.1</small>

> MagikMinds Software Services Pvt Ltd.

- Documented by
- Pranathi Ramineni & Yashwanth Mittapalli & Sri Sandhya Badri

<!-- - Powered by [Magikminds](https://www.magikminds.com/ "Visit our Website for more info") -->
<!-- [GitHub](https://github.com/areknawo/rex) -->
[Get Started](basic.md)

<!-- ![color](#ffffff) -->
