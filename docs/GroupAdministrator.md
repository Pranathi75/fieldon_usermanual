
# Group Admnistrator
<!-- {docsify-ignore} -->
Group administrator will be created by the Super Administrator. Group administrator can mapped to n number of Accounts and one Account have multiple group administrators. So, group administrator can view all the projects and tasks information mapped to the respective group administrator accounts.

<p align="center"> <img width="1000" height="500" src="media/GAA.jpg"> </p>

<div align="center"> 

**`Fig` Group administrator activities**

</div>

<p>Enter valid Group Administartor username and password and click on Sign in.</p>

Login as [Group Administartor ](https://demo.fieldon.com/#/login)

<p align="center"> <img width="1000" height="450" src="media/GA/ga1.png"> </p>

<div align="center"> 

**`Fig.1` Group Administrator login page**

</div>

- After login we can see the Group Administartor page, list of below options in the menu bar.

 - <code>Users</code>	
 - <code>Forms</code>
 - <code>Projects</code>	
 - <code>Task</code>
 - <code>Downloads</code>


<hr>

## Users

1.	On click of Users tab we can see the list of Users available.

<p align="center"> <img width="1000" height="450" src="media/GA/ga2.png"> </p>

<div align="center"> 

**`Fig.2` User list**

</div>

### Preview Users

1.	To preview ‘user’ hover on the user, we can see the buttons.

<p align="center"> <img width="1000" height="450" src="media/GA/ga3.png"> </p>

<div align="center"> 

**`Fig.3` Preview User**

</div>

2.	Click on preview button, view the existing data only in read only fields ,group admin cannot update user information

<p align="center"> <img width="1000" height="450" src="media/GA/ga4.png"> </p>

<div align="center"> 

**`Fig.4`  Preview User data**

</div>

### User search 

1.Enter the user’s name in the search box and click on “ →", it will sort the respective user in the user list

<p align="center"> <img width="1000" height="450" src="media/GA/ga5.png"> </p>

<div align="center"> 

**`Fig.5` Search User**

</div>

## Forms
1.	When clicked on Forms tab, we can see the list of Forms available.
2.	Account Administrator can create Forms. Group admin cannot create/edit the forms.

<p align="center"> <img width="1000" height="450" src="media/GA/ga6.png"> </p>
<div align="center"> 

**`Fig.6` List of Forms**

</div>

### Form Preview
1.	To Preview the Form, go to the form and hover on the form, we can see preview button,
2.	Click on preview button a popup will be open with all the widgets in the form.

<p align="center"> <img width="1000" height="450" src="media/GA/ga7.png"> </p>

<div align="center"> 

**`Fig.7` Preview Forms**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga8.png"> </p>

### Form Export to Excel
1.	We can Export the Form to Excel to get all the widgets of the form available in the form of excel.by clicking excel icon.
2.	We can use fill this excel and upload in Work Assignments to user.

<p align="center"> <img width="1000" height="450" src="media/GA/ga9.png"> </p>

<div align="center"> 

**`Fig.9` Export to excel**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga10.png"> </p>

### Submitted forms
1.	Click on the submitted icon to view the user submitted forms
2.	Once all the records are displayed, Admin can be able to export the data to PDF, EXCEL, Attach as Email, Re-assign data, Go to Map view. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga11.png"> </p>

<div align="center"> 

**`Fig.11` View submitted forms**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga12.png"> </p>

### Export to Excel 
1.	To download the data to Excel format, select the records individually or bulk and click on the ‘Export Excel’ button from tabular view. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga13.png"> </p>

<div align="center"> 

**`Fig.13` Export to excel**

</div>

2.	Once after clicking on excel option, now all the list of fields will be displayed select the green color option. 
3.	The application will display a message saying, ‘Please check the downloaded records in downloads tab’ 

<p align="center"> <img width="1000" height="450" src="media/GA/ga14.png"> </p>
4.	The downloaded Excel appears in this format 

<p align="center"> <img width="1000" height="450" src="media/GA/ga15.png"> </p>

### Attach Email
1.	Administrator can email the records to the Valid Email & can also send the same to another email by adding in the Alternative Email Id which is optional. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga16.png"> </p>

<div align="center"> 

**`Fig.16` Select Email option**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga17.png"> </p>
<div align="center"> 

**`Fig.17` Email generation form**

</div>

2.	Admin can attach records and send to any email Id, Via., Excel, PDF. Successful pop-up will be shown on performing email records.

### Export to PDF 
1.	Group Administrator will be able to select single or multiple records and then click on Pdf option to generate the submitted data to PDF format.

<p align="center"> <img width="1000" height="450" src="media/GA/ga18.png"> </p>

<div align="center"> 

**`Fig.18` Select Export PDf Option**

</div>

2. Once after clicking on pdf option, now all the list of fields will be displayed select the green color option. The application will display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align="center"> <img width="1000" height="450" src="media/GA/ga19.png"> </p>

<p align="center"> <img width="1000" height="450" src="media/GA/ga20.png"> </p>

<div align="center"> 

**`Fig.20`PDF preview**

</div>

### See on Map
1.	select the records individually or multiple records and then click on ‘Inserted Locations’. All the submitted data will be displayed as a marker on map.

<p align="center"> <img width="1000" height="450" src="media/GA/ga21.png"> </p>

<div align="center"> 

**`Fig.21`See on map**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga22.png"> </p>
<div align="center"> 

**`Fig.22`Map preview of inserted location**

</div>

### Filter workflow records 
1.By click on filter workflow records option, it will filter out records between with workflow and without workflow.

<p align="center"> <img width="1000" height="450" src="media/GA/ga23.png"> </p>

<div align="center"> 

**`Fig.23`Filter workflow records**

</div>

### Filter records 
1.By clicking on filter records, it will display the pop menu with fields of  form date, to date, users click on get data it will show the respective filtered data.

<p align="center"> <img width="1000" height="450" src="media/GA/ga24.png"> </p>

<div align="center"> 

**`Fig.24`Filter records**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga25.png"> </p>

### Refresh records 
1. Click on refresh option, it will get refresh the record list and shows newly added records in the list.

<p align="center"> <img width="1000" height="450" src="media/GA/ga26.png"> </p>

<div align="center"> 

**`Fig.26`Refresh records**

</div>
 
### Form Info
1.Group administrator can see the information about the form by clicking on info option

<p align="center"> <img width="1000" height="450" src="media/GA/ga27.png"> </p>
<div align="center"> 

**`Fig.27`Form Info**

</div>

2.It will show the pop-up menu with details ,form name, created by, creation date, No of assignments, No of cases collected, last form modification, last downloaded date, last downloaded by, No of users downloaded.

<p align="center"> <img width="1000" height="450" src="media/GA/ga28.png"> </p>
<div align="center"> 

**`Fig.28`Form Information**

</div>

### Form Filter
1.	It shows the forms with different types, like public and private forms 

<p align="center"> <img width="1000" height="450" src="media/GA/ga29.png"> </p>
<div align="center"> 

**`Fig.29`Form Filter**

</div>

2.	By selecting public in list it filter out public type forms in the list, and it shows with blue colour indication.

<p align="center"> <img width="1000" height="450" src="media/GA/ga30.png"> </p>

<div align="center"> 

**`Fig.30`Form with color indication**

</div>

3.	select private option from filter,it will displays forms with private type with organe color indication,if there are no private forms in list it will show no data available.

<p align="center"> <img width="1000" height="450" src="media/GA/ga31.png"> </p>

## Projects
1.When clicked on projcets tab,we can see  the assiocated projects in the list.

2.Account admin can only create project.

<p align="center"> <img width="1000" height="450" src="media/GA/ga32.png"> </p>

<div align="center"> 

**`Fig.32`Shows project**

</div>

### Project view
1. To preview ‘project’ hover on the project, we can see the options

<p align="center"> <img width="1000" height="450" src="media/GA/ga33.png"> </p>

<div align="center"> 

**`Fig.33`Project preview**

</div>

2. Click on preview button, view the existing data only in read only fields, group admin cannot update project information

<p align="center"> <img width="1000" height="450" src="media/GA/ga34.png"> </p>

<div align="center"> 

**`Fig.34`Shows the existing data of the project**

</div>

### Creation of Project Task

The project task within the project will be created by Group administrator

1. To create “project task” first go to project, click on project task, click on the ellipse button.

2. Click on create project task

<p align="center"> <img width="1000" height="450" src="media/GA/ga35.png"> </p>

<div align="center"> 

**`Fig.35`Create project task**

</div>

3. After clicking on create, the form will open with name, Description, category, workflow, set task date, end date

<p align="center"> <img width="1000" height="450" src="media/GA/ga36.png"> </p>

<div align="center"> 

**`Fig.36`Project task creation form**

</div>

4. Fill the details and click on create button
5. After successful creation we can see a toast message. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga37.png"> </p>

<div align="center"> 

**`Fig.37`Message displayed after successful creation**

</div>

### Edit project task

1. To edit ‘project task’ hover on the project task, we can see the options

<p align="center"> <img width="1000" height="450" src="media/GA/ga38.png"> </p>

<div align="center"> 

**`Fig.38`Edit Project task**

</div>

2.  Click on edit button, view the existing data in read only fields expect end date and category fields and if any changes there make it and click on update.., group admin can update project task information.

<p align="center"> <img width="1000" height="450" src="media/GA/ga39.png"> </p>

<div align="center"> 

**`Fig.39`Update task**

</div>

### Delete project task

1.If you want to delete project task, click on delete icon , the project will  delete only when it is not assign to user/ when it is in progress/when it has records.

2. Group admin can only delete project task.

<p align="center"> <img width="1000" height="450" src="media/GA/ga40.png"> </p>
<div align="center"> 

**`Fig.40`Delete task**

</div>

### Create work assignments

1.	To create work assignment first we have to create project task as it describe as in creation of project task ,later click on the work assignment option,

<p align="center"> <img width="1000" height="450" src="media/GA/ga41.png"> </p>

<div align="center"> 

**`Fig.41`Create work assignment**

</div>

2.	After click it opens the work assignment in project task , click on “+” to create

<p align="center"> <img width="1000" height="450" src="media/GA/ga42.png"> </p>

<div align="center"> 

**`Fig.42`Create work assignment**

</div>

3. It will display the pop-up menu asking to “Do you want to attach  prepop  excel sheet?” with “yes” or “No” options, if admin select “yes” the menu with assignment name, form, frequency, start date, end date, upload sheet field with  choose file button to attach excel file. Fill all the details.

4.  Click on next

5. If admin select “No” then the menu with assignment name, form, frequency, start date, end date, fill all the details

6. Click on create

<p align="center"> <img width="1000" height="450" src="media/GA/ga43.png"> </p>

<div align="center"> 

**`Fig.43`Work assignment creation form**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga44.png"> </p>

<div align="center"> 

**`Fig.44`Click on create-to-create work assignment**

</div>

### Edit work assignment

1. To edit ‘work assignment’ hover on the work assignment, we can see the options

<p align="center"> <img width="1000" height="450" src="media/GA/ga45.png"> </p>

<div align="center"> 

**`Fig.45`Edit work assignment**

</div>

2. Click on edit button, view the existing data  in read only fields expect end date,  if any changes there make it and click on update.., group admin can update work assignment  information. After successful update you will see a toast message.

<p align="center"> <img width="1000" height="450" src="media/GA/ga46.png"> </p>

<div align="center"> 

**`Fig.46`Click on Update to update work assignment**

</div>

### Delete work assignment 
1. To delete work assignment, click on delete icon 

<p align="center"> <img width="1000" height="450" src="media/GA/ga47.png"> </p>

<div align="center"> 

**`Fig.47` Delete work assignment**

</div>

2. Admin cannot delete it, when work assignment was started/when it assigns to user/when work assignment is in progress.it shows a toast message work assignment started  

<p align="center"> <img width="1000" height="450" src="media/GA/ga48.png"> </p>

<div align="center"> 

**`Fig.48`Pop message will appear when assignment cannot be deleted**

</div>

### Submitted Records

1.	By clicking on submitted record options once all the records are displayed, Admin can be able to export the data to PDF, EXCEL, Attach as Email, Re-assign data, Go to Map view.

<p align="center"> <img width="2000" height="450" src="media/GA/ga49.png"> </p>

<div align="center"> 

**`Fig.49`Submitted record page**

</div>

### Export to PDF 

1.	Administrator will be able to select single or multiple records and then click on Pdf option to generate the submitted data to PDF format.

<p align="center"> <img width="1000" height="450" src="media/GA/ga50.png"> </p>

<div align="center"> 

**`Fig.50`Export to PDF**

</div>

2.	Once after clicking on pdf option, now all the list of fields will be displayed select the green color option. The application will display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align="center"> <img width="1000" height="450" src="media/GA/ga51.png"> </p>

<div align="center"> 

**`Fig.51`Select required columns**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga52.png"> </p>

<div align="center"> 

**`Fig.52`Toasted messages will appear**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga53.png"> </p>

<div align="center"> 

**`Fig.53`Record data shown in PDF Format**

</div>

### Download to Excel

1.	To download the data to Excel format, select the records individually or bulk and click on the ‘Export Excel’ button from tabular view.

<p align="center"> <img width="1000" height="450" src="media/GA/ga54.png"> </p>

<div align="center"> 

**`Fig.54`Steps to generate excel file**

</div>

2.	Once after clicking on excel option, now all the list of fields will be displayed select the green color option. The application will display a message saying, ‘Please check the downloaded records in downloads tab’

<p align="center"> <img width="1000" height="450" src="media/GA/ga55.png"> </p>

<div align="center"> 

**`Fig.55`Message displays**

</div>

3.	The downloaded Excel appears in this format

<p align="center"> <img width="1000" height="450" src="media/GA/ga56.png"> </p>

<div align="center"> 

**`Fig.56`Message displays**

</div>

### Attach Email

1.	Administrator can email the records to the Valid Email & can also send the same to another email by adding in the Alternative Email Id which is optional. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga57.png"> </p>

<div align="center"> 

**`Fig.57`Steps to attach mail**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga58.png"> </p>

<div align="center"> 

**`Fig.58`Email generation form**

</div>

2.	Admin can attach records and send to any email Id, Via., Excel, PDF. Successful pop-up will be shown on performing email records.

<p align="center"> <img width="1000" height="450" src="media/GA/ga59.png"> </p>

<div align="center"> 

**`Fig.59`Messaged displayed after successful sent**

</div>

### Re-assign the data

1.	Re-assign function defines that, if any record contains invalid or incomplete data then admin will be able to re-assign the data to any user (if required).

2. To re-assign the record, first select the required record, and click on ‘Re-assign’ button.

<p align="center"> <img width="3000" height="450" src="media/GA/ga60.png"> </p>

<div align="center"> 

**`Fig.60`Steps to re-assign record**

</div>

3.	 Enter the comments  and select to which user the record as to reassign and click on submit

<p align="center"> <img width="1000" height="450" src="media/GA/ga61.png"> </p>

<div align="center"> 

**`Fig.61`Steps to re-assign record**

</div>

4.	Once the record is re-assigned, then the record status will be changed to Reassigned record,

### See on Map

First, select the records individually or multiple records and then click on ‘Inserted Locations’. All the submitted data will be displayed as a marker on map.

<p align="center"> <img width="1000" height="450" src="media/GA/ga62.png"> </p>

<div align="center"> 

**`Fig.62`Steps to see insert location on map**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga63.png"> </p>

<div align="center"> 

**`Fig.63`Map shows inserted records**

</div>

### Pending Records

For pending records hover on the task, by clicking on pending records options it displays pending records for respective task.

<p align="center"> <img width="1000" height="450" src="media/GA/ga64.png"> </p>

<div align="center"> 

**`Fig.64`Steps for pending records**

</div>

### Export to excel

1.	Administrator will be able to export the pending data to excel

First, select the data and then click on Excel option

<p align="center"> <img width="1000" height="450" src="media/GA/ga65.png"> </p>

<div align="center"> 

**`Fig.65`Steps for records to export into excel**

</div>

2.Once after clicking on excel option, the fields will be displayed now click on green color icon.

<p align="center"> <img width="1000" height="450" src="media/GA/ga66.png"> </p>

<div align="center"> 

**`Fig.66`Message will appear**

</div>

3.The exported data in excel will be downloaded in downloads tab.

### Export to PDF
1.	Administrator will be able to export the pending data to PDF.

2.	 First, select the data and then click on PDF option.

<p align="center"> <img width="1000" height="450" src="media/GA/ga67.png"> </p>

<div align="center"> 

**`Fig.67`Steps to export records into PDF**

</div>

3.	Once after clicking on excel option, the fields will be displayed now click on green color icon.

<p align="center"> <img width="1000" height="450" src="media/GA/ga68.png"> </p>

<div align="center"> 

**`Fig.68`Message will appear**

</div>

4.	The exported data in PDF will be downloaded in downloads tab.

### Email Pending Records

1.	Administrator will be able to export the pending data Via Email option.

2.	 First, select the data and then click on Email option.

<p align="center"> <img width="1000" height="450" src="media/GA/ga69.png"> </p>

<div align="center"> 

**`Fig.69`Steps to export records to attach mail**

</div>

3.	Once after clicking on Email option, enter the to & CC valid email Id’s. Now select the required format ‘Pdf, Excel’ and then click on submit button.

<p align="center"> <img width="1000" height="450" src="media/GA/ga70.png"> </p>

<div align="center"> 

**`Fig.70`Steps to export records to attach mail**

</div>

4.	Now select the list of fields and then click on green color option, the application will be 
display a message saying, ‘Email sent successfully’

### Upload New Pending Records
1.	Administrator will be able to upload new records using +Button.

2.	First, click on Add button and now select the choose file option.

3.	From the choose file option select the required excel to upload the new data to the task.

<p align="center"> <img width="1000" height="450" src="media/GA/ga71.png"> </p>

<div align="center"> 

**`Fig.71`Steps to add new records**

</div>

4.	Now select the submit option to upload the attached file and the data will be inserted in the system.

<p align="center"> <img width="1000" height="450" src="media/GA/ga72.png"> </p>

<div align="center"> 

**`Fig.72`Steps to add new records**

</div>

### Assign Users
Administrator will be able to assign the unassigned data to the users.

1.	First, select the unassigned users and then click on ‘users.

<p align="center"> <img width="1000" height="450" src="media/GA/ga73.png"> </p>

<div align="center"> 

**`Fig.73`Steps to assign to users**

</div>

Now select the required user and then click on add user icon and the status will be changed from Unassigned to username.

Administrator will be able to assign the pending records to another user.

1.	First select the record and then click on users.

2.	Select the required user, and then click on user icon.

<p align="center"> <img width="1000" height="450" src="media/GA/ga74.png"> </p>

<div align="center"> 

**`Fig.74`Steps to assign records to another user**

</div>

3.	The first assigned username will be changed to another new user.

### Delete Pending Record

1.	First select the records individually or multiple and then click on delete option.

2.	The selected records will be deleted from the list.

<p align="center"> <img width="1000" height="450" src="media/GA/ga75.png"> </p>

<div align="center"> 

**`Fig.75`Steps to Delete record**

</div>

### Edit  Record
1.	Click on submitted  records button in work assignment, it will display the list of  records which are submitted by mobile user

2.	Tap on “original record”, it will opens the form with user entered data.

3.	Tap on the toggle button to edit the user entered details in the record

<p align="center"> <img width="1000" height="450" src="media/GA/ga76.png"> </p>

<div align="center"> 

**`Fig.76`Steps to edit record**

</div>

4.Group admin can edit the details enter by user if necessary,after necessary changes done click on update .

<p align="center"> <img width="1000" height="450" src="media/GA/ga77.png"> </p>

<div align="center"> 

**`Fig.77`Steps to edit record**

</div>

### View sketching in Record

1.	To view sketching, first we have to open record, click on ellipse, the sketching, and images buttons will appear.

<p align="center"> <img width="1000" height="450" src="media/GA/ga78.png"> </p>

<div align="center"> 

**`Fig.78`Steps to view sketching in record**
</div>

2.	Click on sketching button, it will show sketch  attach by user, admin  cannot  able to edit, if there is no sketch attach to record it shows and message “This  record as no sketching”.

<p align="center"> <img width="1000" height="450" src="media/GA/ga79.png"> </p>

<div align="center"> 

**`Fig.79`Message displays “this record has no sketching**

</div>
 
<p align="center"> <img width="1000" height="450" src="media/GA/ga80.png"> </p>

<div align="center"> 

**`Fig.80`Sketching in record**

</div>
 
### View Attachment in Record
1.To view attachments, first we have to open record, tap on attachments, it shows the user attach files in record.

<p align="center"> <img width="1000" height="450" src="media/GA/ga81.png"> </p>

<div align="center"> 

**`Fig.81`Steps to view attachments**

</div>

2.	If there is no file attach to the record it shows “No data available”.

<p align="center"> <img width="1000" height="450" src="media/GA/ga82.png"> </p>

<div align="center"> 

**`Fig.82`No data available message will appear when no attachments found**

</div>

## Workflow Approval Cycles

We have two types of approval cycles for workflow in FieldOn.

•	Task level workflow

•	Record level workflow

### Task Level Workflow

The “Task Level Workflow” is the one assigned on the “Tasks”, the administrators/moderators in the workflow are responsible to approve the whole task by checking the submissions on it.

<p align="center"> <img width="1000" height="450" src="media/GA/ga83.png"> </p>

<div align="center"> 

**`Fig.83`Selection of workflow**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga84.png"> </p>

<div align="center"> 

**`Fig.84`Selection of workflow level**

</div>

1.	While creating a task, we assign workflow to it as follows ,

2.	Select the type of workflow.

3.  After you have created the task, and assigned the workflow to it, we can now create assignments for this task.

4.	For creation of assignment please check the topic “Work Assignments”.

5.	The data is submitted on these assignments by the field user.

6.	After the data is submitted, the Account Administrator, needs to change the status of the task, to “Workflow Cycle Started”, then the workflow would be triggered and the task would be pending for approval of the first person in the workflow.

7.	Example: The following is the workflow used for depicting the task level workflow.

<p align="center"> <img width="1000" height="450" src="media/GA/ga85.png"> </p>

<div align="center"> 

**`Fig.85`Workflow cycle**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga86.png"> </p>

<div align="center"> 

**`Fig.86`Status of project**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga87.png"> </p>

<div align="center"> 

**`Fig.87`Displays  the  status of project**

</div>

### Workflow Cycle Started

•	As in the above workflow, “Chanda GA” is the first person, he will find this task in the tasks pending for his approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga88.png"> </p>

<div align="center"> 

**`Fig.88`Workflow Notificationt**

</div>

•	On clicking on the above highlighted icon, you can find the recent task “Ballarpur Area” pending for his approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga89.png"> </p>

<div align="center"> 

**`Fig.89`Displays  the approvals projects**

</div>

•	Now if the “Chanda GA” rejects the task, all the work assignments under it and corresponding records in it are reassigned to the users.

•	When users, submit all the reassigned records for all work assignment under the particular task, the workflow starts again, and the task is again pending for approval of “Chanda GA”

•	Now the “Chanda GA” approves and the task goes to next level in the workflow and as you can see in the below screenshot, on approval it would go to “Moderator Chandrapur” (who is a moderator).


<p align="center"> <img width="1000" height="450" src="media/GA/ga90.png"> </p>

<div align="center"> 

**`Fig.90`Shows the next approver person in cycle**

</div>

•	Now the task comes to the next level, which is at “Moderator Chandrapur”.

<p align="center"> <img width="1000" height="450" src="media/GA/ga91.png"> </p>

<div align="center"> 

**`Fig.91`Workflow cycle in moderator**

</div>

•	“Moderator Chandrapur” can accept or reject the task, if he accepts, it goes to the next level, else back to the earlier level.

•	Now on accepting, it goes to “Chanda GA”(Note: Please see the from and to for all levels in the workflow.

<p align="center"> <img width="1000" height="450" src="media/GA/ga92.png"> </p>

<div align="center"> 

**`Fig.92`Approval in moderator**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga93.png"> </p>

<div align="center"> 

**`Fig.93`Approval in group admin**

</div>

•	Again, the “Chanda GA” will be responsible to approve or reject the task.

•	On approving the workflow cycle completes.

<p align="center"> <img width="1000" height="450" src="media/GA/ga94.png"> </p>

<div align="center"> 

**`Fig.94`Shows next Approval**

</div>

### Record Level Workflow

The “Record Level Workflow” is the one assigned on the “Tasks/Forms”, the administrators/moderators in the workflow are responsible to approve the record by checking the submissions on it.

Here we take an example of Record Level Workflow on a form. (Note: We have used same workflow as used in task level workflow)

1.	We assign workflow to a form by editing the form.

<p align="center"> <img width="1000" height="450" src="media/GA/ga95.png"> </p>

<div align="center"> 

**`Fig.95`Assign work flow**

</div>

2.	Whenever any record is submitted for this form, the workflow gets triggered and the record goes to the person at the first level of the workflow. You can find the records submitted for forms by clicking on “Submitted Record” icon.

<p align="center"> <img width="1000" height="450" src="media/GA/ga96.png"> </p>

<div align="center"> 

**`Fig.96`Select the record to be approved**

</div>

3.	Now login as “Chanda GA” and go to the forms records, you will find the record status in RED color. The RED color indicates that the record is pending for the logged in user’s approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga97.png"> </p>

<div align="center"> 

**`Fig.97`Here red in colour shows record is pending to approve**

</div>

4.	On clicking on the status, the record details open in the side panel. There on clicking on the ellipse, you will find accept or reject icons.

5.	On rejecting the record, it goes back to the field user for resubmission.

<p align="center"> <img width="1000" height="450" src="media/GA/ga98.png"> </p>

<div align="center"> 

**`Fig.98`Click on update to approve/reject  record**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga99.png"> </p>

6.	When field user submits the data, again the record comes to the “Chanda GA” for approval.

<p align="center"> <img width="1000" height="450" src="media/GA/ga100.png"> </p>

<div align="center"> 

**`Fig.100`When user submits data  again records  come to  groupadmin**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga101.png"> </p>

<div align="center"> 

**`Fig.101`Message displayed after successful approval**

</div>

7.	Now when “Moderator Chandrapur” logs in, he can find the record in RED color in forms records, waiting for his approval

<p align="center"> <img width="1000" height="450" src="media/GA/ga102.png"> </p>

<div align="center"> 

**`Fig.102`Record in pending for review in moderator as workflow**

</div>

8.	The next level person, “Chanda GA”, now has to accept or reject the record.

<p align="center"> <img width="1000" height="450" src="media/GA/ga103.png"> </p>

<div align="center"> 

**`Fig.103`Here the status of the record change to approved after moderator approval**

</div>


## Workflow History

### Task Level Workflow History

For any task having assigned a workflow, we can see the workflow history, that is, the series of approval or rejection done throughout the workflow cycle.

We need to click on the “Workflow history” icon to fetch these details.

<p align="center"> <img width="1000" height="450" src="media/GA/ga104.png"> </p>
<div align="center"> 

**`Fig.104`Hover on the task to view  task workflow history**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga105.png"> </p>

<div align="center"> 

**`Fig.105`Displays the task workflow history**

</div>

### Record Level Workflow History

We can see the workflow history for a record by opening the record and clicking on the ellipse icon, we get icon to view workflow history.

<p align="center"> <img width="1000" height="450" src="media/GA/ga106.png"> </p>

<div align="center"> 

**`Fig.106`Record level workflow history**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga107.png"> </p>

<div align="center"> 

**`Fig.107`Displays record level workflow history**

</div>

## Tasks

Admin can be able to see the task which are created by the account admin, by click on task tab, it will show the associated task in the list

<p align="center"> <img width="1000" height="450" src="media/GA/ga108.png"> </p>

<div align="center"> 

**`Fig.108`Task tab screen**

</div>

### Preview Task

1.	Admin can preview task (if required) by clicking on ‘preview’ option. Admin can see all the  data in fields and those fields are  read and only fields.

<p align="center"> <img width="1000" height="450" src="media/GA/ga109.png"> </p>

<div align="center"> 

**`Fig.109`Preview task**

</div>

### Submitted Records

1.	Submitted records define to the admin, that all the user submitted records will be displayed. Admin can export the data to PDF, EXCEL.

2.	To get the submitted data click on the required task and then ‘Work assignment’.

<p align="center"> <img width="1000" height="450" src="media/GA/ga110.png"> </p>

<div align="center"> 

**`Fig.110`Click on work assignment**

</div>

3.	Now within the work assignment click on ‘Submitted records.

<p align="center"> <img width="1000" height="450" src="media/GA/ga111.png"> </p>

<div align="center"> 

**`Fig.111`Click on submitted records button to view submitted records**

</div>

4.	All the submitted data by the users for the work assignment will be displayed as a list.

<p align="center"> <img width="1000" height="450" src="media/GA/ga112.png"> </p>

<div align="center"> 

**`Fig.112`Displays the submitted records as list**

</div>

5.	Once all the records are displayed, Admin can be able to export the data to PDF, EXCEL, Attach as Email, Re-assign data, Go to Map view.

<p align="center"> <img width="1000" height="450" src="media/GA/ga113.png"> </p>

<div align="center"> 

**`Fig.113`Various options in submitted record tab**

</div>

### Export to PDF 

1.	Administrator will be able to select single or multiple records and then click on Pdf option to generate the submitted data to PDF format.

<p align="center"> <img width="1000" height="450" src="media/GA/ga114.png"> </p>

<div align="center"> 

**`Fig.114`Various options in submitted record tab**

</div>

2.	Once after clicking on pdf option, now all the list of fields will be displayed select the green color option. The application will display a message saying, ‘Please check the downloaded records in downloads tab’.

<p align="center"> <img width="1000" height="450" src="media/GA/ga115.png"> </p>

<div align="center"> 

**`Fig.115`Message appear after records successful export**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga116.png"> </p>

<div align="center"> 

**`Fig.116`Records data in PDF format**

</div>

### Download to Excel

1.	To download the data to Excel format, select the records individually or bulk and click on the ‘Export Excel’ button from tabular view.

<p align="center"> <img width="1000" height="450" src="media/GA/ga117.png"> </p>

<div align="center"> 

**`Fig.117`Steps to export records to excel**

</div>

2.	Once after clicking on excel option, now all the list of fields will be displayed select the green color option. The application will display a message saying, ‘Please check the downloaded records in downloads tab’

<p align="center"> <img width="1000" height="450" src="media/GA/ga118.png"> </p>

<div align="center"> 

**`Fig.118` Message appears after records successful export**

</div>

3.	The downloaded Excel appears in this format

<p align="center"> <img width="1000" height="450" src="media/GA/ga119.png"> </p>

<div align="center"> 

**`Fig.119` Records data in the excel sheet**

</div>

### Attach Email


1.	Administrator can email the records to the Valid Email & can also send the same to another email by adding in the Alternative Email Id which is optional. 

<p align="center"> <img width="1000" height="450" src="media/GA/ga120.png"> </p>

<div align="center"> 

**`Fig.120` Steps to export records to mail**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga121.png"> </p>

<div align="center"> 

**`Fig.121` Fill all the details in form**

</div>

2.	Admin can attach records and send to any email Id, Via., Excel, PDF. Successful pop-up will be shown on performing email records

<p align="center"> <img width="1000" height="450" src="media/GA/ga122.png"> </p>

<div align="center"> 

**`Fig.122`Message appear after records successful send to mail**

</div>

### See on Map

Select the records individually or multiple records and then click on ‘Inserted Locations’. All the submitted data will be displayed as a marker on map.

<p align="center"> <img width="1000" height="450" src="media/GA/ga123.png"> </p>

<div align="center"> 

**`Fig.123`Steps to see inserted location records on map**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga124.png"> </p>

<div align="center"> 

**`Fig.124`Map show records with inserted location**

</div>

### Pending Records in Tasks

The assigned records for a task will be able to take to excel, pdf.

### Export to excel

1. Administrator will be able to export the pending data to excel.

2. First, select the data and then click on Excel option.

<p align="center"> <img width="1000" height="450" src="media/GA/ga125.png"> </p>

<div align="center"> 

**`Fig.125`Steps to export records to excel**

</div>

3.	Once after clicking on excel option, the fields will be displayed now click on green color icon.

<p align="center"> <img width="1000" height="450" src="media/GA/ga126.png"> </p>

<div align="center"> 

**`Fig.126`Message appears after successful export**

</div>

4.	The exported data in excel will be downloaded in downloads tab.

### Export to PDF

1.	Administrator will be able to export the pending data to PDF.

2.	First, select the data and then click on PDF option.

<p align="center"> <img width="1000" height="450" src="media/GA/ga127.png"> </p>

<div align="center"> 

**`Fig.127`Steps to export records to PDF**

</div>

3.	Once after clicking on excel option, the fields will be displayed now click on green color icon.

<p align="center"> <img width="1000" height="450" src="media/GA/ga128.png"> </p>

<div align="center"> 

**`Fig.128`Message appears after successful export to pdf**

</div>

4.	The exported data in PDF will be downloaded in downloads tab.

### Email Pending Records

1.	Administrator will be able to export the pending data Via Email option.

2.	 First, select the data and then click on Email option.

<p align="center"> <img width="1000" height="450" src="media/GA/ga129.png"> </p>

<div align="center"> 

**`Fig.129`Steps to export records to mail**

</div>

3.	Once after clicking on Email option, enter the to & CC valid email Id’s. Now select the required format ‘Pdf, Excel’ and then click on submit button.
 
<p align="center"> <img width="1000" height="450" src="media/GA/ga130.png"> </p>

<div align="center"> 

**`Fig.130`Fill all the details in the menu**

</div>

4.	Now select the list of fields and then click on green color option, the application will be displaying a message saying, ‘Email sent successfully’.

<p align="center"> <img width="1000" height="450" src="media/GA/ga131.png"> </p>

<div align="center"> 

**`Fig.131`Message appears on successfully export records to mail**

</div>

### Edit Record 

1.	Click on submitted  records button in work assignment, it will display the list of  records which are submitted by mobile user

2.	Tap on “original record”, it will opens the form with user entered data.

3.	Tap on the toggle button to edit the user entered details in the record

<p align="center"> <img width="1000" height="450" src="media/GA/ga132.png"> </p>

<div align="center"> 

**`Fig.132`Steps to edit record**

</div>

4.Group admin can edit the details enter by user if necessary,after necessary changes done click on update .

<p align="center"> <img width="1000" height="450" src="media/GA/ga133.png"> </p>

<div align="center"> 

**`Fig.133`Click on update to saved changes in record**

</div>

### View sketching in Record

1.	To view sketching, first we have to open record, click on ellipse, the sketching, and images buttons will appear.

<p align="center"> <img width="1000" height="450" src="media/GA/ga134.png"> </p>

<div align="center"> 

**`Fig.134`Steps to view sketching in record**

</div>

2.	Click on sketching button, it will show sketch  attach by user, admin  cannot  able to edit, if there is no sketch attach to record it shows and message “This  record as no sketching”.

<p align="center"> <img width="1000" height="450" src="media/GA/ga135.png"> </p>

<div align="center"> 

**`Fig.135`Displays a message when on sketching availabled**

</div>

### View Attachment in  Record

1.	To view attachments, first we have to open record, tap on attachments, it shows the user attach files in record.

<p align="center"> <img width="1000" height="450" src="media/GA/ga136.png"> </p>

<div align="center"> 

**`Fig.136`Steps to view attachments in records**

</div>

2.There is no file attach to the record it shows “No data available”.

<p align="center"> <img width="1000" height="450" src="media/GA/ga137.png"> </p>

<div align="center"> 

**`Fig.137`No data available message displays when no attachments available**

</div>

## Downloads

The “Downloads” tab provides the feasibility to download the record reports, that is both excel and pdf reports.

It has the other details like place of download, time of download etc.

The files under “Downloads” tab gets deleted automatically after 24 hours of download.

<p align="center"> <img width="1000" height="450" src="media/GA/ga138.png"> </p>

<div align="center"> 

**`Fig.138`Shows exported records in PDF and excel file formats**

</div>

## Dashboards

The Dashboards provide the statistical information present under the particular login. 

Throughout all the pages, you can find the dashboard icon present in the right middle of the screen edge.

Under we have three type of entities:

•	Projects

•	Tasks

•	Users

1. Under the “Projects” entity the following are the statistics captured.

<p align="center"> <img width="1000" height="450" src="media/GA/ga139.png"> </p>

<div align="center"> 

**`Fig.139`Dashboard**

</div>

<p align="center"> <img width="1000" height="450" src="media/GA/ga140.png"> </p>

<div align="center"> 

**`Fig.140`Project entity dashboard**

</div>

•	Here Completed project block displays the No of project completed.

•	Pending Project block displays the No of projects in pending state.

•	Projects functionality block displays the total No of the projects under group Admin.

•	Work assignments block displays the total No of work assignments under group Admin.

•	In completed Project list we able to the projects which are completed in the form of the list.

2.	Under the “Tasks” entity the following are the statistics captured.

<p align="center"> <img width="1000" height="450" src="media/GA/ga141.png"> </p>

<div align="center"> 

**`Fig.141`Task entity**

</div>

•	Here Completed project block displays the No of project completed.

•	Pending task block displays the No of the task are in pending state.

•	Task block displays the total No of task created.

3. Under the “Users” entity the following are the statistics captured.

<p align="center"> <img width="1000" height="450" src="media/GA/ga142.png"> </p>

<div align="center"> 

**`Fig.142` User’s entity**

</div>

•	Active users block will display the total No of active users.

•	Users block will displays the total No of users present.

•	In active users block which is in right side, by clicking on ellipse button Admin able to see the active users with Username, Logged Time and Device details in the list.






































































































































